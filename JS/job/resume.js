/**
 * Created by Alex Stolbov with love
 * Date: 19/5/14
 * Email: boorick@gmail.com
 */

define(["app", "relational"], function(JobManager, Relational) {

    JobManager.module("Entities", function(Entities, JobManager, Backbone, Marionette, $, _) {

        Company = Backbone.RelationalModel.extend({
            idAttribute: "id"
        });

        Skill = Backbone.RelationalModel.extend({
            idAttribute: "id"
        });

        Education = Backbone.RelationalModel.extend({
            idAttribute: "id"
        });

        Resume = Backbone.RelationalModel.extend({
            urlRoot: '/v1/resume',
            idAttribute: "id",
            relations: [
                {
                    type: Backbone.HasMany,
                    key: 'companies',
                    relatedModel: Company,
                    reverseRelation: {
                        includeInJSON: 'id'
                    }
                },
                {
                    type: Backbone.HasMany,
                    key: 'skills',
                    relatedModel: Skill,
                    reverseRelation: {
                        includeInJSON: 'id'
                    }
                },
                {
                    type: Backbone.HasMany,
                    key: 'educations',
                    relatedModel: Education,
                    reverseRelation: {
                        includeInJSON: 'id'
                    }
                }
            ],

            initialize: function() {
                //this.on("change", API.eventModelChange());
            }
        });



    });



    var API = {
        getResume: function() {
            var defer = $.Deferred();
            var resume = new Resume();
            resume.fetch({
                success: function(data) {
                    defer.resolve(data);
                },
                error: function(data) {
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },


        getNew: function() {
            return new Resume({
                objectives: null
            });
        },

        addNewCompany: function(data, resume) {
            resume.get('companies').add(data);
        },

        editCompany: function(data, company ) {
           company.set(data);
        },

        addNewEducation: function(data, resume) {
            resume.get('educations').add(data);
        },

        editEducation: function(data, education ) {
            education.set(data);
        },

        updateImage: function(img, id, model) {
           // model.set({image:data}); // ，изображение больше не относится к резюме
            var req = $.ajax({
                type: "PUT",
                url: "/v1/profile/avatar/" + id,
                data: {
                    path: img
                }
            });

            req.done(function(data) {
                model.set({image:data});
            });

        },

        eventModelChange: function() {

        },

        /**
         * Update Applicant name
         * @param name
         * @param model
         */
        updateName: function(name, id) {
            var req = $.ajax({
                type: "PUT",
                url: "/v1/profile/name/" + id,
                data: {
                    name: name
                }
            });
        },

        updateSurname: function(surname, id) {
            var req = $.ajax({
                type: "PUT",
                url: "/v1/profile/surname/" + id,
                data: {
                    surname: surname
                }
            });
        },

        updateSpec: function(spec, id) {
            var req = $.ajax({
                type: "PUT",
                url: "/v1/profile/spec/" + id,
                data: {
                    spec: spec
                }
            });
        },

        updateCity: function(city, id) {
            var req = $.ajax({
                type: "PUT",
                url: "/v1/profile/city/" + id,
                data: {
                    city: city
                }
            });
        }
    };




    JobManager.reqres.setHandler("resume:get", function(id) {
        return API.getResume(id);
    })

    JobManager.reqres.setHandler("resume:new", function() {
        return API.getNew();
    });

    JobManager.reqres.setHandler("resume:add:company", function(data, model) {
        return API.addNewCompany(data, model);
    })

    JobManager.reqres.setHandler("resume:edit:company", function(data, model) {
        return API.editCompany(data, model);
    })

    JobManager.reqres.setHandler("resume:add:education", function(data, model) {
        return API.addNewEducation(data, model);
    })

    JobManager.reqres.setHandler("resume:edit:education", function(data, model) {
        return API.editEducation(data, model);
    })

    JobManager.reqres.setHandler("resume:update:image", function(img, id, model) {
        return API.updateImage(img, id,model);
    })

    JobManager.reqres.setHandler("resume:update:name", function(name, id) {
        return API.updateName(name,id);
    });

    JobManager.reqres.setHandler("resume:update:surname", function(surname, model) {
        return API.updateSurname(surname,model);
    });

    JobManager.reqres.setHandler("resume:update:spec", function(spec, id) {
        return API.updateSpec(spec,id);
    });

    JobManager.reqres.setHandler("resume:update:city", function(city, id) {
        return API.updateCity(city,id);
    });


});