/**
 * Created by Alex Stolbov with love
 * Date: 18/6/14
 * Email: boorick@gmail.com
 */


define(["appadmin"], function(AdminManager) {

    /****************************************************************
     * Define Entity Model
     * RSS News grabber
     ***************************************************************/
    AdminManager.module("Entities", function(Entities, AdminManager, Backbone, Marionette, $, _) {

        Entities.News = Backbone.Model.extend({
            //url: '/v1/news/import'
            //urlRoot: '/v1/news/importe'
        });

        Entities.NewsCollection = Backbone.Collection.extend({
            url: '/v1/news/import',
            model: Entities.News
        });

        /****************************************************************
         * API interface object for RSS Feed model
         *
         * @type {{reload: Function}}
         ***************************************************************/
        var API = {
            reload: function() {
                var defer = $.Deferred();
                var resume = new News();
                resume.fetch({
                    success:function(data) {
                        defer.resolve(data);
                    },
                    error: function(data) {

                    }
                })
                return defer.promise();
            },

            getnew: function() {
                var defer = $.Deferred();
                var news = new Entities.NewsCollection();
                news.fetch({
                    success:function(data) {
                        defer.resolve(data);
                    }
                });
                return defer.promise();
            }

        };


        /****************************************************************
         * Set global handlers for the model
         ***************************************************************/
        AdminManager.reqres.setHandler("news:reload", function() {
            return API.reload();
        })

        AdminManager.reqres.setHandler("news:getnew", function() {
            return API.getnew();
        })

    });


});