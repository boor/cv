/**
 * Created by Alex Stolbov with love
 * Date: 4/8/14
 * Email: boorick@gmail.com
 */

define(["app", "apps/sliders/applicant_slider_view"], function(JobManager, View) {

    JobManager.module("UserApp.ApplSlider", function(ApplSlider, JobManager, Backbone, Marionette, $, _) {

        ApplSlider.Controller = {

            init: function() {
                    var formView = new View.Form({
                        el: '#js-last-applicants'
                    });
            }

        }
    });

    return JobManager.UserApp.ApplSlider.Controller;
});