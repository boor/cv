/**
 * Created by Alex Stolbov with love
 * Date: 4/8/14
 * Email: boorick@gmail.com
 */

define([
    "app",
    "bootstrap"
], function(JobManager) {

    JobManager.module("UserApp.ApplSlider.View", function(View, JobManager, Backbone, Marionette, $, _ ) {
        View.Form = Marionette.ItemView.extend({

            events: {
                "click #js-appl-left" : "clickSlideLeft",
                "click #js-appl-right" : "clickSlideRight"
            },

            initialize: function() {

                $( ".last-applicant-item-container" ).each( function( index, listItem ) {

                });
            },


            clickSlideLeft: function(e) {
                e.stopPropagation();
                e.preventDefault();

                var $firstEl = $( ".last-applicant-item-container").first();
                var $lastEl = $( ".last-applicant-item-container").last();
                var $containerEl = this.$el.find('.last-applicant-row');

                var shift = $firstEl.width() + 40 - 5; // <- поправка на отступы

                var i = 0, step = 6; // множитель сдвига
                $containerEl.offset({left:$containerEl.offset().left - shift});
                //$containerEl.width($containerEl.width() + shift);
                $lastEl.clone().prependTo($containerEl);
                $lastEl.remove();
                var interval = setInterval(function() {
                    $containerEl.offset({left:$containerEl.offset().left + step});
                    i = i + step;

                    if ( i >= shift) {
                        clearInterval(interval);




                    }

                }, 10); // <<<--- interval of timer for animation

            },

            clickSlideRight: function(e) {
                e.stopPropagation();
                e.preventDefault();

                var $firstEl = $( ".last-applicant-item-container").first();
                var $lastEl = $( ".last-applicant-item-container").last();
                var $containerEl = this.$el.find('.last-applicant-row');

                var shift = $firstEl.width() + 40 - 5; // <- поправка на отступы

                var i = shift, step = 6; // множитель сдвига
                var interval = setInterval(function() {
                    $containerEl.offset({left:$containerEl.offset().left - step});
                    i = i - step;

                    if ( i <= 0) {
                        clearInterval(interval);

                        $firstEl.clone().appendTo($containerEl);
                        $firstEl.remove();
                        $containerEl.offset({left:$containerEl.offset().left + shift});
                    }

                }, 10); // <<<--- interval of timer for animation
            }


        });

    });


    return JobManager.UserApp.ApplSlider.View;

});