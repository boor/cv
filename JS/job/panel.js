/**
 * Created by Alex Stolbov with love
 * Date: 21/7/14
 * Email: boorick@gmail.com
 */

define(["jquery"], function() {

    var Panel = {
        stat: undefined,
        panelLeft: undefined,
        panelEl: undefined,
        interval: undefined,
        anchorEl: undefined,
        contentEl: undefined,

        /**
         * Initialization element on the page
         * Triggering outside (see js/require_main.js file)
         */
        init: function() {
            this.panelEl = $('.right-pannel-host');
            $('#js-panel-anchor').on('click', this.clickAnchor);
            this.panelLeft = this.panelEl.offset().left;
            this.anchorEl = $('#js-panel-anchor');
            this.contentEl = $('.page-content');

            if (this.panelEl.hasClass('panel-disable')) {
                this.stat = 'disable';
            }
        },

        /**
         * Click on the open/close element
         * Triggering open/close event
         */
        clickAnchor: function() {
            if (Panel.stat == 'disable') {
                Panel.interval = setInterval(Panel.animateEnable, 10);
            } else {
                Panel.interval = setInterval(Panel.animateDisable, 10);
            }

        },

        /**
         * Opening panel animation
         */
        animateEnable: function() {
            // Stop animation if left offset >= -10 (-10 need for fix shifting)
            if (Panel.panelLeft >= -10) {
                clearInterval(Panel.interval);
                Panel.stat = 'enable';
                Panel.panelEl.removeClass('panel-disable');
                Panel.anchorEl.css('background','rgb(32, 54, 74) url(/images/arrow-left.png) no-repeat scroll 8px 15px');

                // Set/update cookie
                Panel.saveState(true);
            }

            Panel.panelLeft += 10;
            Panel.panelEl.offset({left:Panel.panelLeft});

            // Change Content block width
            Panel.contentEl.css('margin-left', Panel.anchorEl.offset().left + 33);
        },

        /**
         * Closing panel animation
         */
        animateDisable: function() {
            // Stop timer of left offset <= -280 + shift (-10 for fix shifting)
            // -280 need to change manualy if width of the panel will have change
            if (Panel.panelLeft <= -280 + 10) {
                clearInterval(Panel.interval);
                Panel.stat = 'disable';
                Panel.panelEl.addClass('panel-disable');
                Panel.anchorEl.css('background','rgb(32, 54, 74) url(/images/arrow-right.png) no-repeat scroll 8px 15px');

                // Set/update cookie
                Panel.saveState(false);
            }

            Panel.panelLeft -= 10;
            Panel.panelEl.offset({left:Panel.panelLeft});

            // Change Content block width
            Panel.contentEl.css('margin-left', Panel.anchorEl.offset().left + 33);
        },

        /**
         * Save panel state to backend
         *
         * Using POST /v1/setPanelState url
         *
         * @param $state bool current state of panel that need to save
         */
        saveState: function($state) {
            var req = $.ajax({
                type: "POST",
                url: "/v1/setPanelState",
                data: {
                    panel:$state
                }
            });
        }
    }


    return Panel;
});