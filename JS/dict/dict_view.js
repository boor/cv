/**
 * Created by Alex Stolbov with love
 * Date: 9/10/14
 * Email: boorick@gmail.com
 */

define(["app", "semantic"],
    function(DiscussBundle) {

        DiscussBundle.module("DictApp", function(DictApp, DiscussBundle, Backbone, Marionette, $, _) {


            DictApp.View = Marionette.ItemView.extend({
                el: '#js-dict-response',

                events: {

                },

                initialize: function() {
                    this.elSendButton = $('#js-send-btn');
                    this.elInput = $('#js-input');
                    this.elStartText = $('.init-state-text');
                    this.elSpinner = $('.spinner');

                    /** Activate button */
                    this.elSendButton.html('<i class="big search icon"></i>');
                    this.elSendButton.removeClass('loading disabled');

                    /** EVENT on click send button */
                    this.elSendButton.on('click',{view:this}, this.doTranslate);

                    this.elInput.on('keypress', {view:this}, this.pressEnter);

                    this.elStartText.transition('fade up');

                    this.listenTo(this, "dict:newtranslate", this.updateInput);
                    this.listenTo(this, "dict:startload", this.startLoader);
                    this.listenTo(this, "dict:stoptload", this.stopLoader);
                    this.listenTo(this, "dict:clearcontent", this.clearContent);
                },


                doTranslate: function(e) {
                    e.stopPropagation();
                    e.preventDefault();
                    var view = e.data.view;

                    view.startLoader();
                    view.clearContent();

                    var response = DiscussBundle.request('dict:translate', $('#js-input').val());

                    $.when(response).done(function(resp) {
                        view.trigger('dict:showresult', resp.get('data'));
                        $('.bkrs-copy-container').show();
                        view.stopLoader();
                    });
                },

                updateInput: function(text) {
                    this.elInput.val(text);
                },

                startLoader: function() {
                    $('#spinner').show();
                    var opts = {
                        lines: 11, // The number of lines to draw
                        length: 11, // The length of each line
                        width: 7, // The line thickness
                        radius: 28, // The radius of the inner circle
                        corners: 0.9, // Corner roundness (0..1)
                        rotate: 40, // The rotation offset
                        direction: 1, // 1: clockwise, -1: counterclockwise
                        color: '#000', // #rgb or #rrggbb or array of colors
                        speed: 1, // Rounds per second
                        trail: 67, // Afterglow percentage
                        shadow: false, // Whether to render a shadow
                        hwaccel: false, // Whether to use hardware acceleration
                        className: 'spinner', // The CSS class to assign to the spinner
                        zIndex: 2e9, // The z-index (defaults to 2000000000)
                        top: '50%', // Top position relative to parent
                        left: '50%' // Left position relative to parent
                    };
                    var target = document.getElementById('spinner');
                    var spinner = new Spinner(opts).spin(target);
                },

                stopLoader: function() {
                    $('#spinner').hide();
                },

                clearContent: function() {
                    $('.bkrs-copy-container').hide();
                    this.$el.empty();
                },

                pressEnter: function(e) {
                    var keyCode = e.keyCode || e.which;
                    if (keyCode == '13'){
                        e.data.view.elSendButton.trigger('click');
                    }
                }


            });


        });


        return DiscussBundle.DictApp;
    });
