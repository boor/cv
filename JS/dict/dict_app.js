/**
 * Created by Alex Stolbov with love
 * Date: 9/10/14
 * Email: boorick@gmail.com
 */

define(["app", 'apps/dict/dict_controller'], function(DiscussBundle) {

    DiscussBundle.module("DictApp", function (DictApp, DiscussBundle, Backbone, Marionette, $, _) {

        /**
         * Define routes for application
         */
        DictApp.Router = Marionette.AppRouter.extend({
            appRoutes: {
                "serials": "listSerials"
            }
        });


        /**
         * API for application
         */
        var API = {
            // The global method that shows list of the serial numbers
            mainpage: function () {
                DiscussBundle.DictApp.Controller.init();
            }
        }


        DiscussBundle.on("dict:init", function () {
            DiscussBundle.navigate("/");
            API.mainpage();
        });


        /**
         * Adding initializer for module. That function will trigger exactly after the
         * global initializer (for all app)
         */
        DiscussBundle.addInitializer(function () {

            // Init router for application with API that was define above
            //new SerialsApp.Router({
            //    controller: API
            //});

        })

    });
});