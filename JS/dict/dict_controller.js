/**
 * Created by Alex Stolbov with love
 * Date: 9/10/14
 * Email: boorick@gmail.com
 */

define(["app",
        "entities/dict",
        "apps/common/view",
        "apps/dict/dict_view"
    ],
    function (DiscussBundle, dictEntity, CommonViews, dictView )
    {

        DiscussBundle.module("DictApp", function(DictApp, DiscussBundle, Backbone, Marionette, $, _) {

            DictApp.Controller = {

                init: function() {

                    var view = new dictView.View({
                        model: dictEntity
                    });

                    view.on('dict:showresult', function(data) {
                        var self = this;
                        this.$el.html(data);
                        $('.bkrs-copy-container').show();

                        this.$el.find('a').on('click', function(e) {
                            e.preventDefault();
                            //e.stopPropagation();

                            var href = $(this).attr('href');

                            re = new RegExp(/slovo/);

                            if ( re.test(href) ) {
                                var splittext = href.split('=');
                                console.log(splittext);
                                self.trigger('dict:newtranslate',splittext[1]);
                                self.trigger();
                            } else {
                                var splittext = href.split('/');
                                self.trigger('dict:newtranslate',splittext[4]);
                            }

                        });
                    });

                    view.on("dict:newtranslate", function(text) {
                        var self = this;

                        self.trigger('dict:startload');
                        self.trigger('dict:clearcontent');

                        var response = DiscussBundle.request('dict:translate', text);


                        $.when(response).done(function(resp) {
                            self.trigger('dict:stoptload');
                            self.trigger('dict:showresult', resp.get('data'));
                        });
                    });

                }
            }
        });


        return DiscussBundle.DictApp.Controller;
    });

