/**
 * Created by Alex Stolbov with love
 * Date: 24/4/14
 * Email: boorick@gmail.com
 */


/**************************************************************************
 * Defining MAin bundle for the Application
 *************************************************************************/
define([
    "marionette", 'apps/regions/dialog'
], function(Marionette) {


    /**
     * Create main instance for whole application
     */
    var VoIPBundle = new Marionette.Application();

    /**
     * Add main regions
     */
    VoIPBundle.addRegions({
        dboardMonthChartRegion: '#db-month-chart',
        vmregion: "#js-vmapp",
        cdrRegion: "#js-cdr-main-region",
        dialogRegion: Marionette.Region.Dialog.extend({
            el: "#dialog-region"
        })
    })

    VoIPBundle.navigate = function(route, options) {
        options || (options = {});
        Backbone.history.navigate(route, options);
    };

    VoIPBundle.getCurrentRoute = function() {
        return Backbone.history.fragment
    };

    VoIPBundle.on("initialize:after", function() {

        if ( document.getElementById('signin-form') ) {
            require(["apps/signin/controller"], function(Controller) {
                Controller.init();
            });
        }

        /**
         * If have month chart on page
         */
        if ( document.getElementById('db-month-chart') ) {
            require(["apps/charts/month-chart/controller"], function(Controller) {
                Controller.init();
            });
        }

        if ( document.getElementById('server-list-container') ) {
            require(["apps/servers/servers_controller"], function(Controller) {
                Controller.init();
            });
        }


        /**
         * Init app for voicemail page
         */
        if ( window.location.pathname == '/voicemail' ) {
            if (Backbone.history) {
                Backbone.history.start();

                if (this.getCurrentRoute() === '') {
                    require(["apps/voicemail/app","apps/voicemail/controller"], function(Controller) {
                        VoIPBundle.trigger("vm:init");
                    });
                }
            }
        }

        /**
         * Init app for cdr pages
         */
        if ( window.location.pathname == '/cdr' ) {
            if (Backbone.history) {
                Backbone.history.start();

                if (this.getCurrentRoute() === '') {
                    require(["apps/cdr/app","apps/cdr/controller"], function(Controller) {
                        VoIPBundle.trigger("cdr:init");
                    });
                }
            }
        }



    });

    return VoIPBundle;

});




