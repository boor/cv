/**
 * Created by Alex Stolbov with love
 * Date: 10/9/14
 * Email: boorick@gmail.com
 */

define(["app","chart"], function(VoIPBundle, Chart) {

    VoIPBundle.module("ChartApp.Month", function(Month, VoIPBundle, Backbone, Marionette, $, _ ) {


        Month.View = Marionette.ItemView.extend({


            initialize: function() {


                var ctx = this.$el.find('canvas').get(0).getContext("2d");
                ctx.canvas.height = 200;

                this.MonthChart = new Chart(ctx);


                var data = {
                    labels: this.options.chardata.labels,
                    datasets: [
                        {
                            label: "My First dataset",
                            fillColor: "rgba(47, 220, 60, 0.50)",
                            strokeColor: "rgba(38, 173, 47, 0.85)",
                            highlightFill: "rgba(38, 173, 47, 0.85)",
                            highlightStroke: "rgba(38, 173, 47, 0.85)",
                            data: this.options.chardata.data
                        }
                    ]
                };

                var options = {
                    //showScale: true
                    scaleShowLabels : false,
                    tooltipTemplate: "<%=value%>"

                }

                this.MonthChart.Bar(data,options);

            }


        });


    });

    return VoIPBundle.ChartApp.Month.View;
});
