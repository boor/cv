/**
 * Created by Alex Stolbov with love
 * Date: 10/9/14
 * Email: boorick@gmail.com
 */

define(["app"], function(VoIPBundle) {

    VoIPBundle.module("Entities", function(Entities, VoIPBundle, Backbone, Marionette, $, _) {


        Entities.ChartData = Backbone.Model.extend({
            url: "/v1/charts/chart-month"
        })


    });

    var API = {
        getData: function() {
            var data = new VoIPBundle.Entities.ChartData();

            var defer = $.Deferred();

            var req = $.ajax({
                type: "GET",
                url: "/v1/charts/chart-month/"
            });

            req.done(function(response) {
                defer.resolve(response);
            });

            return defer.promise();
        }
    }

    VoIPBundle.reqres.setHandler("chart:get:data", function() {
        return API.getData();
    });

    return VoIPBundle.Entities;
});