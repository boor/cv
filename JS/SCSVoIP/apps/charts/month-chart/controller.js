/**
 * Created by Alex Stolbov with love
 * Date: 10/9/14
 * Email: boorick@gmail.com
 */

define(["app", "apps/charts/month-chart/view", "apps/charts/month-chart/model"],
    function(VoIPBundle, View, Model) {

    VoIPBundle.module("ChartApp.Month", function(Month, VoIPBundle, Backbone, Marionette, $, _) {

        Month.Controller = {

            init: function() {

                var fetchingData = VoIPBundle.request("chart:get:data");

                $.when(fetchingData).done(function(data) {
                    var view = new View({
                        el: '#db-month-chart',
                        chardata: data
                    });

                    //view.chartdata = data;
                });

            }

        }
    });

    return VoIPBundle.ChartApp.Month.Controller;
});