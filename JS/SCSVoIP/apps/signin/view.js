/**
 * Created by Alex Stolbov with love
 * Date: 15/8/14
 * Email: boorick@gmail.com
 */

define([
    "app",
    "bootstrap"
], function(VoIPBundle) {

    VoIPBundle.module("UserApp.Signin.View", function(View, VoIPBundle, Backbone, Marionette, $, _ ) {

        View.Form = Marionette.ItemView.extend({

            events: {
                "submit form" : "formSubmit"
            },

            /**
             * Initialization of view
             */
            initialize: function() {
                this.$elForm = this.$el.find('#signin-form');
                this.$elName = this.$el.find('#inputName');
                this.$elPassword = this.$el.find('#inputPassword');
                this.$elBtn = this.$el.find('.btn-primary');
                this.$elMsg = this.$el.find('#js-message');


                /** Enable button */

                this.$elBtn.removeClass('disabled');
            },

            /**
             * EVENT on submit signin form
             */
            formSubmit: function(e) {
                var self = this;

                /** Prevent default actions, we will not use it */
                e.preventDefault();
                e.stopPropagation();

                /** Reset error message if displayed */
                this.clearErrorMessage();

                /** Grab the values */
                var name = this.$elName.val();
                var pass = this.$elPassword.val();

                var req = $.ajax({
                    type: "POST",
                    url: "/signin",
                    data: {name:name,password:pass},
                    beforeSend: function( xhr ) {
                        self.$elBtn.button('loading');
                    }
                });

                /** When the query was finished without any errors */
                req.done(function(response) {
                    self.$elBtn.button('reset');
                    self.SigninSuccess(response);
                });

                /** When query was finished with have a fail */
                req.fail(function() {
                    self.$elBtn.button('reset');
                    self.SigninFailed(null);
                });
            },

            /**
             * Backend answer OK
             */
            SigninSuccess: function( response ) {
                if ( response.error ) {
                    this.doShakeAnimation();
                    this.setErrorMessage(response.message);
                } else {
                    window.location = '/dashboard';
                }
            },

            /**
             * Backend query was filed
             * @param message Error message with the reason
             * @constructor
             */
            SigninFailed: function( message ) {
                this.doShakeAnimation();
                this.setErrorMessage('Something wrong with server');
            },

            /**
             * Shake the form for visual effect
             */
            doShakeAnimation: function () {
                this.$el.addClass('animated shake');
                this.$el.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(e) {
                    $(e.target).removeClass('animated shake');
                });
            },

            /**
             * Set Error message on Signin form
             * @param message Text of message
             */
            setErrorMessage: function ( message ) {
                this.$elMsg.text(message).show();
            },

            /**
             * Clear error message from the signin form
             */
            clearErrorMessage: function() {
                this.$elMsg.empty().hide();
            }


        });

    });

    return VoIPBundle.UserApp.Signin.View;

});
