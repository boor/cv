/**
 * Created by Alex Stolbov with love
 * Date: 15/8/14
 * Email: boorick@gmail.com
 */

define(["app", "apps/signin/view"], function(VoIPBundle, View) {

    VoIPBundle.module("UserApp.Signin", function(Signin, VoIPBundle, Backbone, Marionette, $, _) {

        Signin.Controller = {

            init: function() {
                //require(["entities/signin"], function(Model) {

                    var formView = new View.Form({
                        el: '.signin-form-container'
                    });

                    //JobManager.authFormRegion.show(authFormView);
                //});

            }

        }
    });

    return VoIPBundle.UserApp.Signin.Controller;
});