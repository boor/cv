/**
 * Created by Alex Stolbov with love
 * Date: 17/10/14
 * Email: boorick@gmail.com
 */

define(["app", "apps/servers/servers_common_view", "entities/server"],
    function(VoIPBundle, viewCommon, ServerModel) {

    VoIPBundle.module("ServersApp", function(ServersApp, VoIPBundle, Backbone, Marionette, $, _) {

        ServersApp.Controller = {

            /**
             * Initialization View
             */
            init: function() {
                var view = new viewCommon.Common({
                    controller: this
                });
            },

            /**
             * Initialization Socket with each server
             *
             * @param ip Address of target server
             */
            initServer: function(ip) {
                var model = VoIPBundle.request('servers:connect:new', ip);
            }


        }
    });

    return VoIPBundle.ServersApp.Controller;
});


