/**
 * Created by Alex Stolbov with love
 * Date: 17/10/14
 * Email: boorick@gmail.com
 */

define(["app",
    "bootstrap"], function(VoIPBundle) {
    VoIPBundle.module("ServersApp.View", function(View, VoIPBundle, Backbone, Marionette, $, _) {


        View.Common = Marionette.ItemView.extend({
            el: '#server-list-container',

            events: {

            },

            /**
             * Initializing View
             *
             * Remind you that we have one view for all table region witch containing
             * all rows with corresponded servers.
             */
            initialize: function() {
                var self = this;


                this.listenTo(this, 'connect:online', this.setStatusOnline);
                this.listenTo(this, 'connect:fail', this.setStatusError);
                this.listenTo(this, 'update:latency', this.setLatency);

                /**
                 *  Finding server rows, inxlude ip addresses and trying create a model
                 *  for each server. The request 'servers:connect:new' returning deferred
                 *  object and we need to processing .done event for get model (already
                 *  connected to server side socket) or error object with error description
                 */
                this.$el.find('tr').each(function(k,v) {
                    var ip_addr =  $(v).find('#js-server-addr').text();

                    var result = VoIPBundle.request('servers:connect:new', ip_addr, self);

                    /** Processing deferred object when it done */
                    $.when(result).done(function(data) {
                        //self.setErrorConnect(data.ipaddr, data.message);
                    });
                });
            },


            /**
             * Set Error label on table row
             *
             * @param ipaddr IP Address of server witch need to mark
             * @param msg Error description
             */
            setStatusError: function(ipaddr, msg) {
                this.$el.find("tr[data-ip='"+ipaddr+"']")
                    .find('#js-server-label span').removeClass('label-default')
                    .addClass('label-danger')
                    .text('CONNECTION ERROR!');

                this.$el.find("tr[data-ip='"+ipaddr+"'] td img").hide();
            },

            /**
             * Set ON LINE status on the server table row
             *
             * @param m Model of connected server
             */
            setStatusOnline: function(m) {
                var ipaddr = m.get('ipaddr');
                this.$el.find("tr[data-ip='"+ipaddr+"']")
                    .find('#js-server-label span').removeClass('label-default')
                    .addClass('label-success')
                    .text('ON LINE');
            },

            setLatency: function(m,l) {
                var ipaddr = m.get('ipaddr');
                this.$el.find("tr[data-ip='"+ipaddr+"']")
                    .find('#js-server-latency')
                    .html(l + ' ms');
            }

        });



    });


    return VoIPBundle.ServersApp.View;
});