/**
 * Created by Alex Stolbov with love
 * Date: 28/8/14
 * Email: boorick@gmail.com
 */

define(["app", "apps/voicemail/view_main", "apps/voicemail/view_comments"], function(VoIPBundle, View, ViewComments) {

    VoIPBundle.module("VMApp", function(VMApp, VoIPBundle, Backbone, Marionette, $, _) {

        VMApp.Controller = {

            init: function() {
                require(["entities/voicemail"], function(Model) {

                var layoutView = new View.Layout({
                    model: Model.Voicemail
                });

                    //VoIPBundle.vmregion.show(layoutView);
                    VoIPBundle.vmregion.show(layoutView);
                });

            },

            list: function(filelist, model) {

                // Define
                var Message = Backbone.Model.extend({});

                var Messages = Backbone.Collection.extend({
                    model: Message
                });

                // Creating item collections
                var messageCollection = new Messages(filelist);

                // Creating Collection View of voice messages
                var filelistView = new View.Messages({
                    collection: messageCollection
                })

                VoIPBundle.vmregion.show(filelistView);


                /**
                 * Event on click Comment button on list
                 */
                filelistView.on("itemview:contact:edit",function(childView,model) {

                    var viewLayout = new ViewComments.LayoutContainer({
                        modal: true,
                        vmmsgid: model.get('id')
                    });


                    var fetchingComments = VoIPBundle.request("vm:comments:list", model.get('id'));

                    $.when(fetchingComments).done(function(comments) {

                        var viewComments = new ViewComments.Comments({
                            collection: comments
                        });

                        viewLayout.on('show', function() {
                            viewLayout.commentsRegion.show(viewComments);
                        });

                        /**
                         * Delete item from collection
                         */
                        comments.on("comment:del", function(model) {
                            model.destroy({
                                data: {id:model.id},
                                processData: true
                            });
                        });

                        VoIPBundle.dialogRegion.show(viewLayout);
                    });
                });

            }

        }
    });

    return VoIPBundle.VMApp.Controller;
});

