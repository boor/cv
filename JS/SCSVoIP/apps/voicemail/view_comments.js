/**
 * Created by Alex Stolbov with love
 * Date: 3/9/14
 * Email: boorick@gmail.com
 */

define(["app",
    "tpl!apps/voicemail/templates/comments.tpl",
    "tpl!apps/voicemail/templates/comment-container.tpl",
    "tpl!apps/voicemail/templates/comment-item.tpl",
    "bootstrap"], function(VoIPBundle, template, containerTpl, itemTpl) {
    VoIPBundle.module("VMApp.View", function(View, VoIPBundle, Backbone, Marionette, $, _) {

        View.LayoutContainer = Marionette.Layout.extend({
            template: template,

            regions: {
               commentsRegion: "#comments-region"
            },

            events: {
                "click #js-add-comment" : "clickAddButton"
            },

            initialize: function() {
                this.title = 'View comments';
            },

            onShow: function() {
                    this.$el.dialog({
                        modal:true,
                        title: this.title,
                        width: 800,
                        maxHeight: 560
                    });
            },

            clickAddButton: function(e) {
                e.preventDefault();
                e.stopPropagation();
                var $text = this.$el.find('#commentInput');

                var comment = new VoIPBundle.Entities.VMComment({
                    vmid: this.options.vmmsgid,
                    comment: $text.val()
                });

                this.commentsRegion.currentView.collection.add(comment);
                $text.val('');
            }

        });





        View.Comment = Marionette.ItemView.extend({
            template: itemTpl,
            tagName: 'div',
            className: 'vm-dialog-comm-item',

            events: {
                "click #js-del-comment" : 'clickDelete'
            },

            clickDelete: function(e) {
                e.preventDefault();
                e.stopPropagation();

                this.model.collection.trigger('comment:del', this.model);

            }
        });

        View.Comments = Marionette.CollectionView.extend({
            itemView: View.Comment,

            initialize: function() {
                this.listenTo(this.collection, "add", this.eventCollectionChanged);
            },

            eventCollectionChanged: function() {
                this.collection.save();
            }
        });


    });


    return VoIPBundle.VMApp.View;
});