/**
 * Created by Alex Stolbov with love
 * Date: 28/8/14
 * Email: boorick@gmail.com
 */


define(["app",
    "tpl!apps/voicemail/templates/default.tpl",
    "tpl!apps/voicemail/templates/message.tpl",
    "bootstrap", "audiojs"], function(VoIPBundle, defaultTpl, messageTpl) {
    VoIPBundle.module("VMApp.View", function(View, VoIPBundle, Backbone, Marionette, $, _) {

        View.Layout = Marionette.Layout.extend({
            template: defaultTpl,


            initialize: function() {

                // Click on box link (right side voicemail box lists
                // Its out of module view and we using common jQuery binding
                // In this case changing execution context, be careful
                $('.js-box-item').on('click', {model:this.model}, this.clickMailbox);

                // Bind model event listeners
                this.listenTo(this.model, 'vm:connection:getinfo', this.eventConnectionGetinfo);
                this.listenTo(this.model, 'vm:connection:start', this.eventConnectionStart);
                this.listenTo(this.model, 'vm:connection:successful', this.eventConnectionSuccessful);
                this.listenTo(this.model, 'vm:connection:error', this.eventConnectionError);
            },

            onShow: function() {
                // Show 'Choose' caption when module loaded
                this.$el.find('i').addClass('animated fadeInRight')
                    .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                        $(this).removeClass('animated bounceInRight');
                    });
            },


            /**
             * Click on mailbox name in left side list
             *
             * Context of function is changed and not belongs to View module
             * @param e
             */
            clickMailbox: function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                VoIPBundle.request("vm:connect",id, e.data.model);
            },

            /**
             * Change status text according with model events
             */
            eventConnectionGetinfo: function() {
                this.$el.find('.js-inline-loader').show();
                this.$el.find('#js-def-msg').text('Getting info...');
            },

            /**
             * Change status text according with model events
             */
            eventConnectionStart: function() {
                this.$el.find('.js-inline-loader').show();
                this.$el.find('#js-def-msg').text('Connecting to server...');
            },

            /**
             * Change status text according with model events
             * Connection Succesfull. Hide spinner and update the text
             */
            eventConnectionSuccessful: function() {
                this.$el.find('.js-inline-loader').hide();
                this.$el.find('#js-def-msg').text('Connected!').addClass('text-success');
            },

            /**
             * Change status text according with model events
             * Connection failed. Hide spinner and update the text
             */
            eventConnectionError: function() {
                this.$el.find('.js-inline-loader').hide();
                this.$el.find('#js-def-msg').text('Connection failed!').addClass('text-danger');
            }



        });

        /*************************************************************/

        /**
         * VIEW of each item in the list of voice messages
         * @type {*|void|extend|extend|extend|extend}
         */
        View.Message = Marionette.ItemView.extend({
            template: messageTpl,
            tagName: 'div',
            className: 'panel panel-default',

            events: {
                'click .js-btn-comment' : "clickComments",
                'click .js-btn-remove' : "clickRemove"
            },

            /**
             * Click on the Comment button at list of messages
             * @param e
             */
            clickComments: function(e) {
                e.preventDefault();
                e.stopPropagation();

                this.trigger("contact:edit", this.model);
            },

            /**
             * Click on the Remove button at list of messages
             * @param e
             */
            clickRemove: function(e) {
                e.preventDefault();
                e.stopPropagation();

                VoIPBundle.request("vm:delete",this.model);
            }


        });

        /*************************************************************/

        /**
         * VIEW Collection of voice messages (collection view)
         * @type {*|void|extend|extend|extend|extend}
         */
        View.Messages = Marionette.CollectionView.extend({
            itemView: View.Message,
            tagName: 'div',
            className: 'panel-group',

            events: {
            },

            onShow: function() {
                // Initialize AudioJS elements on the page
                audiojs.createAll();
            }
        })

    });


    return VoIPBundle.VMApp.View;
});
