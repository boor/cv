/**
 * Created by Alex Stolbov with love
 * Date: 28/8/14
 * Email: boorick@gmail.com
 */

define(["app"], function(VoIPBundle) {

    VoIPBundle.module("VMApp", function(VMApp, VoIPBundle, Backbone, Marionette, $, _) {

        /**
         * Define routes for application
         */
        VMApp.Router = Marionette.AppRouter.extend({
            appRoutes: {
                "serials": "listSerials"
            }
        });

        /**
         * API for application
         */
        var API = {

            init: function() {
                VMApp.Controller.init();
            },

            // The global method that shows list of the serial numbers
            listSerials: function() {
                SerialsApp.List.Controller.listSerials();
            }
        }


        VoIPBundle.on("vm:init", function() {
            //SerialManager.navigate("serials");
            API.init();
        });




    });
});
