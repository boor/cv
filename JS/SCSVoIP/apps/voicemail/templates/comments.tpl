<div class="vm-dialog-comm-container">

    <div id="comments-region">

    </div>

    <hr/>

    <div>
        <form role="form">

            <div class="form-group">
                <label for="commentInput">Add new comment</label>
                <textarea class="form-control" rows="3" id="commentInput"></textarea>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button class="btn btn-primary pull-right" id="js-add-comment"><span class="glyphicon glyphicon-plus"></span> Add</button>
                </div>
            </div>

        </form>

    </div>


</div>