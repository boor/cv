    <div class="panel-heading">
        <a data-toggle="collapse" data-parent="#accordion" href="#p<%=id%>"><h4 class="panel-title list-callerid">
            <%=callerid%>


                <div class="btn-group pull-right btn-list btn-group-xs">
                    <button type="button" class="btn btn-primary js-btn-comment">
                        <span class="glyphicon glyphicon-comment"></span>
                    </button>
                    <button type="button" class="btn btn-danger js-btn-remove">
                        <span class="glyphicon glyphicon-remove"></span>
                    </button>
                </div>
        <span class="pull-right"><%=duration%>s</span>
        <small class="pull-right"><%=date%></small> </h4>

        </a>

    </div>
    <div id="p<%=id%>" class="panel-collapse collapse">
        <div class="panel-body panel-body-fix">
            <audio src="//172.16.13.7:3000/voicemail/<%=user%>/<%=wav1%>" preload="none" />
        </div>
    </div>