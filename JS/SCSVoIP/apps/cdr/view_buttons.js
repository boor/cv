/**
 * Created by Alex Stolbov with love
 * Date: 18/9/14
 * Email: boorick@gmail.com
 */

define(["app",
    "tpl!apps/cdr/templates/buttons.tpl",
    "bootstrap", "audiojs"], function(VoIPBundle, template) {
    VoIPBundle.module("CDRApp.View", function(View, VoIPBundle, Backbone, Marionette, $, _) {

        /**
         * VIEW BUttons
         * @type {*|void|extend|extend|extend|extend}
         */
        View.Buttons = Marionette.ItemView.extend({
            template: template,
            tagName: 'div',
            className: '',

            initialize: function() {
                this.listenTo(this, 'update:state', this.eventUpdateState);
            },

            onShow: function() {
                this.eventUpdateState(this.options.collection)
            },


            eventClickPageBtn: function(e) {
                e.preventDefault();
                e.stopPropagation();

                var page = $(e.target).text();
                var date = $('#from-date').val();

                // Triggering event in controller
                e.data.model.trigger('gotoPage', page, date);
            },

            /**
             * Update pagination button panel according with current state
             *
             * @param collection
             */
            eventUpdateState: function(collection) {
                  //  console.log(collection);

                /** clean state */
                $("ul.pagination li[data-page]").remove();

                var self = this;
                var pageBtns = '';

                    for (i=1; i<=collection.last_page; i++) {
                        pageBtns += '<li data-page="'+i+'"><a href="#">'+i+'</a></li>';
                    }


                /** Add page buttons to DOM */
                this.$el.find('#js-page-step-bw').parent().after(pageBtns);

                /** Make current page active */
                $("ul.pagination li[data-page='"+collection.current_page+"'] ").addClass('active');

                /** Bind click event on page buttons */
                $("ul.pagination li a").on('click', {model:self}, self.eventClickPageBtn);
            }

        });


    });


    return VoIPBundle.CDRApp.View;
});

