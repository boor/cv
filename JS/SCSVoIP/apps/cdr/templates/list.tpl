    <td><%=calldate%></td>
    <td><%=src%></td>
    <td><%=dst%></td>
    <td>
        <% if (disposition == 'ANSWERED') { %>
        <span class="label label-success">ANSWERED</span>
        <% } else if (disposition == 'NO ANSWER') { %>
        <span class="label label-warning">NO ANSWER</span>
        <% } else if (disposition == 'BUSY') { %>
        <span class="label label-danger">BUSY</span>
        <% } else { %>
        <span class="label label-default"><%=disposition%></span>
        <% } %>
    </td>
    <td><%=duration%></td>
    <td><%=billsec%></td>