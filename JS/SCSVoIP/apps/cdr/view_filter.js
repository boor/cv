/**
 * Created by Alex Stolbov with love
 * Date: 22/9/14
 * Email: boorick@gmail.com
 */


define(["app",
    "tpl!apps/cdr/templates/filter.tpl",
    "bootstrap", "jquery-ui"], function(VoIPBundle, filterTpl) {
    VoIPBundle.module("CDRApp.View", function(View, VoIPBundle, Backbone, Marionette, $, _) {

        /**
         * VIEW of Filter
         * @type {*|void|extend|extend|extend|extend}
         */
        View.Filter = Marionette.ItemView.extend({
            template: filterTpl,
            tagName: 'div',
            className: 'cdr-filter-container',

            events: {
                "input #from-date" : 'eventDateInput'
            },

            initialize: function() {


                var now = new Date();
                var dd = now.getDate();
                var mm = now.getMonth()+1; //January is 0!
                var yyyy = now.getFullYear();

                if(dd<10){
                    dd='0'+dd
                }
                if(mm<10){
                    mm='0'+mm
                }

                // Add current_date to the main CDR collection
                this.options.collection.currend_date = dd +'.' + mm + '.' + yyyy;
            },

            onShow: function() {
                var self = this;
                this.$elDateFrom = this.$el.find('input#from-date');

                this.$elDateFrom.datepicker({
                    dateFormat: "dd.mm.yy",
                    onSelect: function(d,e) {
                        self.onSelectDate(d,e,self)
                    }
                });

                this.$elDateFrom.val(this.options.collection.currend_date);
            },


            eventDateInput: function(e) {
                var val = $(e.target).val();
            },

            onSelectDate: function(date, elem, view) {
                view.trigger("dateChanged",date);
            }




        });


    });


    return VoIPBundle.CDRApp.View;
});

