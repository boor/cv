/**
 * Created by Alex Stolbov with love
 * Date: 24/9/14
 * Email: boorick@gmail.com
 */

define(["app",
    "tpl!apps/cdr/templates/loader.tpl",
    "bootstrap", "jquery-ui"], function(VoIPBundle, template) {
    VoIPBundle.module("CDRApp.View", function(View, VoIPBundle, Backbone, Marionette, $, _) {

        /**
         * VIEW of Filter
         * @type {*|void|extend|extend|extend|extend}
         */
        View.Loader = Marionette.ItemView.extend({
            template: template,
            tagName: 'div',
            className: ''

        });


    });


    return VoIPBundle.CDRApp.View;
});
