/**
 * Created by Alex Stolbov with love
 * Date: 17/9/14
 * Email: boorick@gmail.com
 */

define(["app"], function(VoIPBundle) {

    VoIPBundle.module("CDRApp", function(CDRApp, VoIPBundle, Backbone, Marionette, $, _) {

        /**
         * Define routes for application
         */
        CDRApp.Router = Marionette.AppRouter.extend({
            appRoutes: {
                "list": "callList"
            }
        });

        /**
         * API for application
         */
        var API = {

            callList: function() {
                CDRApp.Controller.init();
            }

        }


        VoIPBundle.on("cdr:init", function() {
            //VoIPBundle.navigate("list");
            API.callList();
        });


        //VoIPBundle.addInitializer(function() {
            // Init router for application with API that was define above
            //new CDRApp.Router({
            //    controller: API
            //});
        //})


    });
});
