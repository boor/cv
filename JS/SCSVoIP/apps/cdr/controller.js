/**
 * Created by Alex Stolbov with love
 * Date: 17/9/14
 * Email: boorick@gmail.com
 */

define(["app", "apps/cdr/view_list", "apps/cdr/view_buttons", "apps/cdr/view_filter", "apps/cdr/view_loader"],
    function(VoIPBundle, ViewList, ViewButtons, ViewFilter, ViewLoader) {

    VoIPBundle.module("CDRApp", function(CDRApp, VoIPBundle, Backbone, Marionette, $, _) {

        CDRApp.Controller = {

            init: function() {
                require(["entities/cdr"], function(Model) {

                    var viewLoader = new ViewLoader.Loader();
                    VoIPBundle.cdrRegion.show(viewLoader);

                    var fetchingModel = VoIPBundle.request('cdr:list');

                    $.when(fetchingModel).done(function(collection) {

                        var viewList = new ViewList.Items({
                            collection: collection
                        });

                        // Init base layout view
                        var viewLayout = new ViewList.Layout();

                        // Init Filter view
                        var viewFilter = new ViewFilter.Filter({
                            collection: collection
                        });

                        // Init buttons view
                        var viewButtons = new ViewButtons.Buttons({
                            collection: collection
                        });


                        /**
                         * When Layout showing fill the regions
                         */
                        viewLayout.on("show", function() {
                            viewLayout.tableRegion.show(viewList);
                            viewLayout.buttonsRegion.show(viewButtons);
                            viewLayout.buttonsTopRegion.show(viewFilter);
                        });

                        //Show Layout to main regio
                        VoIPBundle.cdrRegion.show(viewLayout);

                        /**
                         * fetch defined page of CDR list
                         */
                        viewButtons.on("gotoPage", function(page, date) {
                            //Fetchig new collection from backend
                            var fetchingModel = VoIPBundle.request('cdr:list', page, date);

                            $.when(fetchingModel).done(function(data) {
                                var viewList = new ViewList.Items({
                                    collection: data
                                });

                                viewLayout.tableRegion.show(viewList);
                                viewButtons.trigger('update:state',data);
                            });
                        });


                        viewFilter.on("dateChanged", function(date) {
                            //Fetching new collection
                            var fetchingModel = VoIPBundle.request('cdr:list', null, date);

                            $.when(fetchingModel).done(function(data) {
                                var viewList = new ViewList.Items({
                                    collection: data
                                });

                                viewLayout.tableRegion.show(viewList);
                                viewButtons.trigger('update:state',data);
                            });
                        });


                    }); //when


                });

            }

        }
    });

    return VoIPBundle.CDRApp.Controller;
});
