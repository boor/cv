/**
 * Created by Alex Stolbov with love
 * Date: 17/9/14
 * Email: boorick@gmail.com
 */

define(["app",
    "tpl!apps/cdr/templates/list.tpl",
    "tpl!apps/cdr/templates/list_container.tpl",
    "tpl!apps/cdr/templates/list_layout.tpl",
    "bootstrap", "audiojs"], function(VoIPBundle, listTpl, containerTpl, layoutTpl) {
    VoIPBundle.module("CDRApp.View", function(View, VoIPBundle, Backbone, Marionette, $, _) {


        View.Layout = Marionette.Layout.extend({
            template: layoutTpl,

            regions: {
                tableRegion: "#tableRegion",
                buttonsTopRegion: "#buttonsTopRegion",
                buttonsRegion: "#buttonsRegion"
            }
        })




        /**
         * VIEW
         * @type {*|void|extend|extend|extend|extend}
         */
        View.Item = Marionette.ItemView.extend({
            template: listTpl,
            tagName: 'tr',
            className: ''

        });

        /*************************************************************/

        /**
         * VIEW
         * @type {*|void|extend|extend|extend|extend}
         */
        View.Items = Marionette.CompositeView.extend({
            tagName: 'table',
            className: 'table table-hover',
            template: containerTpl,
            itemView: View.Item,
            itemViewContainer: 'tbody'

        })

    });


    return VoIPBundle.CDRApp.View;
});
