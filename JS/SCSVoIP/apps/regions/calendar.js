/**
 * Created by Alex Stolbov with love
 * Date: 22/9/14
 * Email: boorick@gmail.com
 */

define(["marionette","jquery-ui"],function(Marionette){

    Marionette.Region.Calendar = Marionette.Region.extend({

        onShow: function(view){
            //this.listenTo(view, "dialog:close", this.closeDialog);

            var self = this;

            this.$el..datepicker();
        },

        //closeDialog: function() {
        //    this.stopListening();
        //    this.close();
        //    this.$el.dialog("destroy");
        //}

    });

    return Marionette.Region.Calendar;
});