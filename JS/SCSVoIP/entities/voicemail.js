/**
 * Created by Alex Stolbov with love
 * Date: 28/8/14
 * Email: boorick@gmail.com
 */

define(["app"], function(VoIPBundle) {

    VoIPBundle.module("Entities", function(Entities, VoIPBundle, Backbone, Marionette, $, _) {


        var Voicemail = Backbone.Model.extend();

        Message = Backbone.Model.extend({
            //idAttribute: "id"
        })


        Entities.Voicemail = new Voicemail({
        });



        Entities.Voicemail.on('change:address', function() {
            API.connect(this);
        });

        /**
         * When socket attribute updating we need rebind all events, because socket attributes
         * changing only when we connecting to another asterisk server
         */
        Entities.Voicemail.on('change:socket', function() {

            /** Bind socket events to Socket API */
            this.get('socket').on('connect', SocketAPI.connect);
            this.get('socket').on('filelist:update', SocketAPI.updFilelist);
            this.get('socket').on('vm:delete:successful', SocketAPI.deleteMessage);

        });

        /**********************************************************************/

        Entities.VMComment = Backbone.Model.extend({
            url: '/v1/voicemail/comment'
        })

        Entities.VMCommentCollection = Backbone.Collection.extend({
            url: '/v1/voicemail/comments',
            model: Entities.VMComment,
            save: function() {
                var collection = this;
                Backbone.sync('update', collection);
            }
        })

        /**********************************************************************/
        var APIComments = {
            /**
             * Return list of comments for voice message
             * @param id ID of voicemessage
             * @returns {*} Defered object
             */
            getList: function(id) {
                var comments = new Entities.VMCommentCollection();

                var defer = $.Deferred();
                comments.fetch({
                    data: $.param({ msgid: id}),
                    processData: true,
                    success: function(data) {
                        defer.resolve(data);
                    }
                });

                return defer.promise();
            }
        }

        /**********************************************************************/
        var API = {

            /**
             * Requesting remote server details and set it to model
             *
             * @param id
             * @param model
             */
            getInfo: function(id, model) {
                model.trigger('vm:connection:getinfo');

                var req = $.ajax({
                    type: "GET",
                    url: "/v1/voicemail/getinfo/" + id,
                    beforeSend: function( xhr ) {

                    }
                });

                req.done(function(response) {
                    model.set(response);
                });
            },

            /**
             * Load websocket script from remote server, create websocket object and set it to the model
             *
             * @param model
             */
            connect: function(model) {
                model.trigger('vm:connection:start');

                require(['//172.16.13.7:3000/socket.io/socket.io.js'], function(io) {

                    /** Create web socket */
                    var socket = io.connect('172.16.13.7:3000');

                    /** Set created socket to the model attribute */
                    Entities.Voicemail.set({socket:socket})

                }, function(err) {          // <-- error callback when loading filed
                    Entities.Voicemail.trigger('vm:connection:error');
                });
            },

            delete: function(model) {
                Entities.Voicemail.get('socket').emit('vm:delete', model.get('title'), model.get('id'));
            }
        }

        /**********************************************************************/
        var SocketAPI = {

            /**
             * WebSocket connected
             */
            connect: function() {
                var model = Entities.Voicemail;
                var socket = model.get('socket');

                /** Trigger event to model. Event of models grubbing by view */
                model.trigger('vm:connection:successful');
                socket.emit('get:list');
            },

            updFilelist: function(filelist) {
                var model = Entities.Voicemail;

                VoIPBundle.VMApp.Controller.list(filelist, Entities.Voicemail);
            },


            deleteMessage: function(filename, id) {
                var $elem = VoIPBundle.vmregion.currentView.$el.find('#p' + id).parent();
                $elem.addClass('animated fadeOutUp');
                $elem.one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                    $elem.hide();
                });
            }


        }


        /**********************************************************************/
        VoIPBundle.reqres.setHandler("vm:connect", function(id, model) {
            return API.getInfo(id, model);
        });

        VoIPBundle.reqres.setHandler("vm:comments:list", function(id) {
            return APIComments.getList(id);
        });

        VoIPBundle.reqres.setHandler("vm:delete", function(model) {
            return API.delete(model);
        });


    });

    return VoIPBundle.Entities;
});