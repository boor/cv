/**
 * Created by Alex Stolbov with love
 * Date: 17/9/14
 * Email: boorick@gmail.com
 */

define(["app"], function(VoIPBundle) {

    VoIPBundle.module("Entities", function(Entities, VoIPBundle, Backbone, Marionette, $, _) {


        Entities.CDRItem = Backbone.Model.extend({
            url: '/v1/cdr/item'
        })

        Entities.CDRItemCollection = Backbone.Collection.extend({
            url: '/v1/cdr/items',
            model: Entities.CDRItem,
            parse: function(response){
                this.current_page = response.current_page;
                this.from = response.from;
                this.last_page = response.last_page;
                this.per_page = response.per_page;
                this.to = response.to;
                this.total = response.total;
                return response.data;
            }
        })


        /**********************************************************************/

        var API = {
            /**
             * Fetch CDR list from backend
             * @returns {*}
             */
            getList: function(page, date) {
                var list = new Entities.CDRItemCollection();

                if ( !page ) page = 1;
                if ( !date ) date = '';

                var defer = $.Deferred();
                list.fetch({
                    data: $.param({ page:page, date:date}),
                    processData: true,
                    success: function(data) {
                        defer.resolve(data);
                    }
                });

                return defer.promise();
            }
        }


        /**********************************************************************/
        VoIPBundle.reqres.setHandler("cdr:list", function(page, date) {
            return API.getList(page, date);
        });


    });

    return VoIPBundle.Entities;
});