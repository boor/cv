/**
 * Created by Alex Stolbov with love
 * Date: 20/10/14
 * Email: boorick@gmail.com
 */

define(["app"], function(VoIPBundle) {

    VoIPBundle.module("Entities", function(Entities, VoIPBundle, Backbone, Marionette, $, _) {

        Entities.Server = Backbone.Model.extend({
            url: '/v1/server'
        });




        /**********************************************************************/
        var APIModel = {
            init: function(ip, view) {

                var server = new Entities.Server({
                    ipaddr: ip
                });

                var defer = $.Deferred();

                /**
                 * Triggering function on connect like this because a bug
                 */
                server.on('change:socket', function() {
                    this.get('socket').on('connect', function() {
                        SocketAPI.onConnectSucc(server, view)
                    });

                    this.get('socket').on('pong', function(latency) {
                        SocketAPI.onPong(server, view, latency)
                    })
                })



                require(['//' + ip + ':3000/socket.io/socket.io.js'], function(io) {

                    /** Create new WebSocket */
                    var socket = io.connect('172.16.13.7:3000');

                    /** and set socket instance to the model attribute */
                    server.set({socket:socket});


                }, function(err) {

                    /** When connect error return error message object */
                    SocketAPI.onConnectError(ip, view)
                });

                return defer.promise();
            }
        }

        /**********************************************************************/

        var SocketAPI = {
            onConnectSucc: function(model, view) {
                view.trigger('connect:online', model);
            },

            onConnectError: function(ip, view) {
                view.trigger('connect:fail', ip);
            },

            onPong: function(model, view, latency) {
                view.trigger('update:latency', model, latency);
            }
        }

        /**********************************************************************/
        VoIPBundle.reqres.setHandler("servers:connect:new", function(ip, view) {
            return APIModel.init(ip, view);
        });

    });

    return VoIPBundle.Server;
});