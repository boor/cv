/**
 * Created by Alex Stolbov with love
 * Date: 5/11/14
 * Email: boorick@gmail.com
 */

define([
    "app",
    "bootstrap"
], function(AppBundle) {

    AppBundle.module("Slider", function(Slider, AppBundle, Backbone, Marionette, $, _ ) {

        Slider.View = Marionette.ItemView.extend({

            events: {
                "click #js-slider-prev-btn" : "clickPrev",
                "click #js-slider-next-btn" : "clickNext"
            },

            /**
             * View init
             */
            initialize: function() {

                this.$line = $('#js-dotted-line');
                this.$prevBtn = $('#js-slider-prev-btn');
                this.$nextBtn = $('#js-slider-next-btn');
                this.$raceInfo = $('#js-race-info');

                /** Display slider elements */
                $('#bottom-slider-container').fadeIn();

                var port_w = $(document).width(),
                    port_h = $(document).height(),
                    slider_line_top = port_h - 130,
                    prev_btn_top = slider_line_top + 50,
                    prev_btn_left = 20,
                    next_btn_top = prev_btn_top,
                    next_btn_left = port_w - 40,
                    info_top = slider_line_top + 20,
                    info_left = 100;

                this.$line.offset({top : slider_line_top});
                this.$prevBtn.offset({ top : prev_btn_top, left : prev_btn_left });
                this.$nextBtn.offset({ top : next_btn_top, left : next_btn_left });
                this.$raceInfo.offset({ top : info_top, left : info_left });

                this.$line.addClass('animated fadeInUp');

                this.drawEvents();

            },

            /**
             * Draw slide animation
             */
            drawEvents: function() {
                var self = this;

                var shift_x = 0;
                var shift_rate = 200;

                this.collection.comparator = function( model ) {
                    return model.get( 'date' );
                }

                self.$line.after('<div id="js-point-container"></div>');
                $('#js-point-container').offset({top : self.$line.position().top});

                this.collection.sort();

                /** Processing each event in collection and put point on the line */
                _(this.collection.models).each(function(event) {
                    var date = new Date(event.get('date')*1000);
                    var rawdate = date.getMonth() + 1;
                    var dd = date.getDate() < 10 ? '0' + date.getDate() : date.getDate();
                    var mm = rawdate < 10 ? '0' + rawdate : rawdate;
                    var yy = date.getFullYear();
                    var strDate = yy + '/' + mm + '/' + dd;

                    var $point = $('<div data-id="'+event.get('id')+'" status="'+event.get('active')+'"></div>');

                    event.get('active') ? $point.addClass('event-point-active') : $point.addClass('event-point');


                    $('#js-point-container').append($point.offset({left: shift_x}));

                    var $point = $("div[data-id='"+event.get('id')+"']");

                    if (event.get('active')) {
                        var $label = $('<span class="point-label-active" data-id="'+event.get('id')+'">'+strDate+'</span>');
                        $point.after($label.offset({
                            left: $point.position().left - 35
                        }));
                    } else {
                        var $label = $('<span class="point-label" data-id="'+event.get('id')+'">'+strDate+'</span>');
                        $point.after($label.offset({
                            left: $point.position().left - 23
                        }));
                    }


                    shift_x += shift_rate;
                });

                var act_shift = $("div[status='true']").position().left;
                var container_left = ($(document).width() / 2) - act_shift;

                $('#js-point-container').offset({left:container_left});


            },

            /**
             * Click on the Prev button emplementation
             */
            clickPrev: function() {
                var index = this.collection.indexOf(this.collection.getActive().models[0]);
                var last_index = this.collection.size() - 1;

                /** If the point not first and not last */
                if ( index > 0 && index < last_index || index == last_index ) {
                    this.collection.models[index].set({active:false});
                    this.collection.models[index - 1].set({active:true});
                    var active_left = this._getPointEl(this.collection.models[index].get('id')).position().left;
                    var prev_left = this._getPointEl(this.collection.models[index-1].get('id')).position().left;

                    /** Act the moving animation */
                    this._moveLeft(active_left - prev_left);

                    this._updateEventInfo(this.collection.models[index - 1].get('id'), 'left');
                }
            },

            /**
             * Click on the Next button event emplementation
             */
            clickNext: function() {
                var index = this.collection.indexOf(this.collection.getActive().models[0]);
                var last_index = this.collection.size() - 1;

                /** If the point not first and not last */
                if ( index > 0 && index < last_index || index == 0) {
                    this.collection.models[index].set({active:false});
                    this.collection.models[index + 1].set({active:true});
                    var active_left = this._getPointEl(this.collection.models[index].get('id')).position().left;
                    var next_left = this._getPointEl(this.collection.models[index+1].get('id')).position().left;

                    /** Act the moving animation */
                    this._moveRight(next_left - active_left);

                    this._updateEventInfo(this.collection.models[index+1].get('id'), 'right');
                }
            },

            /**
             * Return current state of the current state point (html element) ID
             *
             * @param id
             * @returns {*|jQuery|HTMLElement}
             * @private
             */
            _getPointEl: function(id) {
                return $("div[data-id='"+id+"']");
            },

            /**
             * Move slider to the left direction
             *
             * @param shift
             * @private
             */
            _moveLeft: function(shift) {
                var self = this;
                var $container =  $('#js-point-container');
                var pos = $container.position().left;
                var stop = pos + shift;
                var step = 10;

                self._startMove();

                self.moveInterval = setInterval(function() {
                    if (pos < stop) {
                        $container.offset({left:pos})
                        pos = pos + step;
                    } else {
                        clearInterval(self.moveInterval);
                        self._stopMove();
                    }
                }, 10);
            },

            /**
             * Move slider to the right direction
             *
             * @param shift
             * @private
             */
            _moveRight: function(shift) {
                var self = this;
                var $container =  $('#js-point-container');
                var pos = $container.position().left;
                var stop = pos - shift;
                var step = 10;

                self._startMove();

                self.moveInterval = setInterval(function() {
                    if (pos > stop) {
                        $container.offset({left:pos})
                        pos = pos - step;
                    } else {
                        clearInterval(self.moveInterval);
                        self._stopMove();
                    }
                }, 10);
            },

            /** When slider start move */
            _startMove: function() {
                $('div.event-point-active')
                    .removeClass('event-point-active')
                    .addClass('event-point');

                $('span.point-label-active')
                    .removeClass('point-label-active')
                    .addClass('point-label');

            },

            /**
             * When slider stop move
             * */
            _stopMove: function() {
                _(this.collection.getActive().models).each(function(model) {
                    var $point = $("div[data-id='"+model.get('id')+"']");
                    var $label = $("span[data-id='"+model.get('id')+"']");

                    $point
                        .removeClass('event-point')
                        .addClass('event-point-active');

                    $label
                        .removeClass('point-label')
                        .addClass('point-label-active');

                    /** Fix label position */
                    $label.offset({
                        left : $point.offset().left + Math.floor($point.width() / 2) - Math.floor($label.width() / 2)
                    })
                });
            },

            /**
             * Updating the captions on the slider
             * Clear state styles, fix positions
             *
             * @param id
             * @param direction
             * @private
             */
            _updateEventInfo: function(id, direction) {
                var $container = $('.race-info-container');
                var $picture = $('.race-info-image');
                var $title = $('#race-info-title');
                var $link = $('#race-info-link');

                if (direction === 'left') {
                    $container.addClass('animated fadeOutRight')
                        .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                            $container.removeClass('animated fadeOutRight');
                        });
                } else {
                    $container.addClass('animated fadeOutLeft')
                        .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                            $container.removeClass('animated fadeOutLeft');
                        });
                }


                var fetchEvent = AppBundle.request("events:getinfo", id);

                $.when(fetchEvent).done(function(res) {
                    $picture.attr('src', '/maps/' + res.get('map'))
                    $title.text(res.get('title'));

                    if (direction === 'left') {
                        $container.addClass('animated fadeIn')
                            .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                                $container.removeClass('animated fadeOutRight');
                            });
                    } else {
                        $container.addClass('animated fadeIn')
                            .one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
                                $container.removeClass('animated fadeOutLeft');
                            });
                    }

                });
            }



        });

    });

    return AppBundle.
        Slider.View;

});

