/**
 * Created by Alex Stolbov with love
 * Date: 5/11/14
 * Email: boorick@gmail.com
 */

define(["app", "apps/slider/slider_view"], function(AppBundle, View) {

    AppBundle.module("Slider", function(Slider, AppBundle, Backbone, Marionette, $, _) {

        Slider.Controller = {

            init: function() {
                require(["apps/slider/slider_model"], function(Model) {

                    var fetchEvents = AppBundle.request("events:fetch");

                    $.when(fetchEvents).done(function(response) {
                        var view = new View({
                            el: '#bottom-slider-container',
                            collection: response
                        });
                    });

                }); // require model
            }

        }
    });

    return AppBundle.Slider.Controller;
});