/**
 * Created by Alex Stolbov with love
 * Date: 5/11/14
 * Email: boorick@gmail.com
 */

define(["app"], function(AppBundle) {

    AppBundle.module("Slider.Model", function(Model, AppBundle, Backbone, Marionette, $, _) {


        Model.Eventt = Backbone.Model.extend({
            url: '/api/event'
        })

        Model.Events = Backbone.Collection.extend({
            url: '/api/events',
            model: AppBundle.Slider.Model.Eventt,

            getActive: function() {
                filtered = this.filter(function(model) {
                    return model.get("active") === true;
                });
                return new Model.Events(filtered);
            }
        });


    });

    var API = {
        fetch: function() {
            /** Defer object */
            var defer = $.Deferred();

            /** Create new onbject of model */
            var collection = new AppBundle.Slider.Model.Events();

            /** Fetching data and resolve deferred object if successful */
            collection.fetch({
                success: function(data) {
                    defer.resolve(data);
                }
            });

            return defer.promise();
        },

        getInfo: function(id) {
            var defer = $.Deferred();

            var event = new AppBundle.Slider.Model.Eventt();

            /** Fetching data and resolve deferred object if successful */
            event.fetch({
                data: $.param({id:id}),
                processData: true,
                success: function(data) {
                    defer.resolve(data);
                }
            });

            return defer.promise();
        }
    }



    AppBundle.reqres.setHandler("events:fetch", function() {
        return API.fetch();
    });

    AppBundle.reqres.setHandler("events:getinfo", function(id) {
        return API.getInfo(id);
    });

    return AppBundle.Slider.Model;
});