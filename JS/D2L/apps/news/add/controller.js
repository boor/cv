/**
 * Created by Alex Stolbov with love
 * Date: 4/11/14
 * Email: boorick@gmail.com
 */

define(["app","jquery-ui"], function(AppBundle) {

    AppBundle.module("AddNews", function(AddNews, AppBundle, Backbone, Marionette, $, _) {

        AddNews.Controller = {

            init: function() {

                $('#dateElem').datepicker({
                    dateFormat: "yy/mm/dd"
                });

            }

        }
    });

    return AppBundle.AddNews.Controller;
});