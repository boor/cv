/**
 * Created by Alex Stolbov with love
 * Date: 2/11/14
 * Email: boorick@gmail.com
 */

define([
    "app",
    "bootstrap",
    "knob"
], function(AppBundle) {

    AppBundle.module("Countdown", function(Countdown, AppBundle, Backbone, Marionette, $, _ ) {

        Countdown.View = Marionette.ItemView.extend({

            /**
             * View init
             */
            initialize: function() {

                /** View elements */
                this.$elDays = $('#js-days');
                this.$elHours = $('#js-hours');
                this.$elMinutes = $('#js-minutes');
                this.$elSeconds = $('#js-seconds');

                /** Knob parameters for the date counter */
                var params_day = {
                    width: 120,
                    fgColor: "#c70000",
                    inputColor: "#eee",
                    bgColor: "#eee",
                    skin: "tron",
                    thickness: ".2",
                    displayPrevious: true,
                    min: 0,
                    max: 30,
                    readOnly: true
                };

                /** Knob parameters for the Hours counters */
                var params_hours = {
                    width: 120,
                    fgColor: "#c70000",
                    inputColor: "#eee",
                    bgColor: "#eee",
                    skin: "tron",
                    thickness: ".2",
                    displayPrevious: true,
                    min: 0,
                    max: 23,
                    readOnly: true
                };

                /** Knob parameters for the Minutes counters */
                var params_time = {
                    width: 120,
                    fgColor: "#c70000",
                    inputColor: "#eee",
                    bgColor: "#eee",
                    skin: "tron",
                    thickness: ".2",
                    displayPrevious: true,
                    min: 0,
                    max: 59,
                    readOnly: true
                };

                /** Initialize counter elements */
                this.$elDays.knob(params_day);
                this.$elHours.knob(params_hours);
                this.$elMinutes.knob(params_time);
                this.$elSeconds.knob(params_time);

                $('#js-days').fadeIn();
                $('#js-hours').fadeIn();
                $('#js-minutes').fadeIn();
                $('#js-seconds').fadeIn();

                /** Add caption to DAYS counter */
                this.$elDays
                    .after('<span id="js-days-counter">Days</span>')
                    .offset({top: this.$elDays.offset().top + 8});
                var daysCaptionEl = $('#js-days-counter');
                daysCaptionEl
                    .addClass('counter-caption')
                    .offset({
                        top: this.$elDays.offset().top - 13,
                        left: this.$elDays.offset().left + this.$elDays.width() / 2 - daysCaptionEl.width() / 2
                    });

                /** Add caption to the HOURS counter */
                this.$elHours
                    .after('<span id="js-hours-counter">Hours</span>')
                    .offset({top: this.$elHours.offset().top + 8});
                var hoursCaptionEl = $('#js-hours-counter');
                hoursCaptionEl
                    .addClass('counter-caption')
                    .offset({
                        top: this.$elHours.offset().top - 13,
                        left: this.$elHours.offset().left + this.$elHours.width() / 2 - hoursCaptionEl.width() / 2
                    });

                /** Add caption to the MINUTES counter */
                this.$elMinutes
                    .after('<span id="js-minutes-counter">Minutes</span>')
                    .offset({top: this.$elMinutes.offset().top + 8});
                var minutesCaptionEl = $('#js-minutes-counter');
                minutesCaptionEl
                    .addClass('counter-caption')
                    .offset({
                        top: this.$elMinutes.offset().top - 13,
                        left: this.$elMinutes.offset().left + this.$elMinutes.width() / 2 - minutesCaptionEl.width() / 2
                    });

                /** Add caption to the SECONDS counter */
                this.$elSeconds
                    .after('<span id="js-seconds-counter">Seconds</span>')
                    .offset({top: this.$elSeconds.offset().top + 8});
                var secondsCaptionEl = $('#js-seconds-counter');
                secondsCaptionEl
                    .addClass('counter-caption')
                    .offset({
                        top: this.$elSeconds.offset().top - 13,
                        left: this.$elSeconds.offset().left + this.$elSeconds.width() / 2 - secondsCaptionEl.width() / 2
                    });



                /** Update the counters according with the a current state */
                this.updateCounters();

                /** Bind model events */
                this.listenTo(this.model, 'change:inSeconds', this.updateCounters);

                /** Start countdown */
                this.startCountdown();

            },

            /**
             * Start counting down
             */
            startCountdown: function() {
                var self = this;

                /** Create 1 sec interval and bind function */
                this.Interval = setInterval(function () {
                    self.timer()
                }, 1000);

            },

            /**
             * Function for the interval
             * Calling every second (see @startCountdown() )
             */
            timer: function() {
                this.model.set({
                    inSeconds: this.model.get('inSeconds') - 1
                });
            },

            /**
             * Update state of counter states
             */
            updateCounters: function() {
                var delta = this.model.get('inSeconds');

                /** calculate (and subtract) whole days */
                var days = Math.floor(delta / 86400);
                delta -= days * 86400;

                /** calculate (and subtract) whole hours */
                var hours = Math.floor(delta / 3600) % 24;
                delta -= hours * 3600;

                /** calculate (and subtract) whole minutes */
                var minutes = Math.floor(delta / 60) % 60;
                delta -= minutes * 60;

                /** what's left is seconds */
                var seconds = delta % 60;

                /** set the state */
                this.$elDays.val(days).trigger('change');
                this.$elHours.val(hours).trigger('change');
                this.$elMinutes.val(minutes).trigger('change');
                this.$elSeconds.val(seconds).trigger('change');
            }


        });

    });

    return AppBundle.Countdown.View;

});