/**
 * Created by Alex Stolbov with love
 * Date: 2/11/14
 * Email: boorick@gmail.com
 */

define(["app", "apps/countdown/countdown_view"], function(AppBundle, View) {

    AppBundle.module("Countdown", function(Countdown, AppBundle, Backbone, Marionette, $, _) {

        Countdown.Controller = {

            init: function() {
                require(["entities/countdown"], function(Model) {

                    var fetchingCounter = AppBundle.request("countdown:init");

                    $.when(fetchingCounter).done(function(response) {
                        var formView = new View({
                            el: '.next-race-container',
                            model: response
                        });
                    });

                });
            }

        }
    });

    return AppBundle.Countdown.Controller;
});