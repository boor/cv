/**
 * Created by Alex Stolbov with love
 * Date: 14/11/14
 * Email: boorick@gmail.com
 */

define(["app"], function(AppBundle) {

    AppBundle.module("Background.Model", function(Model, AppBundle, Backbone, Marionette, $, _) {


        Model.Image = Backbone.Model.extend();

        Model.Images = Backbone.Collection.extend({
            url: '/api/images',
            model: AppBundle.Background.Model.Image
        });


    });

    var API = {
        getBGCollection: function() {
            /** Defer object */
            var defer = $.Deferred();

            /** Create new onbject of model */
            var collection = new AppBundle.Background.Model.Images();

            /** Fetching data and resolve deferred object if successful */
            collection.fetch({
                success: function(data) {
                    defer.resolve(data);
                }
            });

            return defer.promise();
        }
    }



    AppBundle.reqres.setHandler("get:collection:background", function() {
        return API.getBGCollection();
    });


    return AppBundle.Background.Model;
});