/**
 * Created by Alex Stolbov with love
 * Date: 14/11/14
 * Email: boorick@gmail.com
 */

define([
    "app",
    "bootstrap"
], function(AppBundle, Model) {

    AppBundle.module("Countdown", function(Countdown, AppBundle, Backbone, Marionette, $, _ ) {

        Countdown.View = Marionette.ItemView.extend({

            /**
             * View init
             */
            initialize: function() {
                var self = this;

                this.$cont1 = $('#bg-layer-one');
                this.$cont2 = $('#bg-layer-two');

                this.state = 1;

                this.mainInterval = setInterval(function () {
                    self.tickTimer()
                }, 6000);


            },

            tickTimer: function() {
                if ( this.state == 1 ) {
                    this.changeToTwo();
                } else {
                    this.changeToOne();
                }
            },

            changeToTwo: function() {
                var self = this;

                if ( this.state == 1 ) {
                    this.$cont2.fadeIn(1000);
                    this.$cont1.fadeOut(1000, function() {
                        self.$cont1.css('background-image','url(/images/bg/'+self.getRandom().get('filename')+')');
                    });
                    this.state = 2;


                    //console.log('url(/images/bg/'+this.getRandom().get('filename')+')');
                }
            },

            changeToOne: function() {
                var self = this;

                if ( this.state == 2 ) {
                    this.$cont1.fadeIn(1000);
                    this.$cont2.fadeOut(1000, function() {
                        self.$cont2.css('background-image','url(/images/bg/'+self.getRandom().get('filename')+')');
                    });
                    this.state = 1;

                }
            },

            getRandom: function() {
                var length = _.random(0, this.collection.length-1);

                return this.collection.models[length];
            }



        });

    });

    return AppBundle.Countdown.View;

});

