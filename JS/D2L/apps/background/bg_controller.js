/**
 * Created by Alex Stolbov with love
 * Date: 14/11/14
 * Email: boorick@gmail.com
 */

define(["app", "apps/background/bg_view"], function(AppBundle, View) {

    AppBundle.module("Background", function(Background, AppBundle, Backbone, Marionette, $, _) {

        Background.Controller = {

            init: function() {
                require(["apps/background/bg_model"], function(Model) {

                    /** Get image collection */
                    var fetchingCollection = AppBundle.request('get:collection:background');

                    $.when(fetchingCollection).done(function(res) {
                        var view = new View({
                            collection: res
                        });
                    });

                    });


            }

        }
    });

    return AppBundle.Background.Controller;
});