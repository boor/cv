/**
 * Created by Alex Stolbov with love
 * Date: 24/10/14
 * Email: boorick@gmail.com
 */


/**************************************************************************
 * Defining MAin bundle for the Application
 *************************************************************************/
define([
    "marionette",
], function(Marionette) {


    /**
     * Create main instance for whole application
     */
    var AppBundle = new Marionette.Application();

    /**
     * Add main regions
     */
    AppBundle.addRegions({

    })

    /**
     * Navigate page to given route
     *
     * @param route
     * @param options
     */
    AppBundle.navigate = function(route, options) {
        options || (options = {});
        Backbone.history.navigate(route, options);
    };

    /**
     * Return current route
     *
     * @returns {S.fragment|Backbone.History.fragment}
     */
    AppBundle.getCurrentRoute = function() {
        return Backbone.history.fragment
    };


    /**
     * After initialize main application
     */
    AppBundle.on("initialize:after", function() {
        var self = this;

        if ( document.getElementById('signin-form') ) {
            require(["apps/signin/controller"], function(Controller) {
                Controller.init();
            });
        }


        /** If have countdown container on the page */
        if ( document.getElementById('countdown-container') ) {
            require(["apps/countdown/countdown_controller"], function(Controller) {
                Controller.init();
            });
        }

        /** If have Slider container on the page */
        if ( document.getElementById('bottom-slider-container') ) {
            require(["apps/slider/slider_controller"], function(Controller) {
                Controller.init();
            });
        }

        /** Dynamic background */
        if ( document.getElementById('bg-layer-one') && document.getElementById('bg-layer-two') ) {
            require(["apps/background/bg_controller"], function(Controller) {
                Controller.init();
            });
        }


        /** Add News item page in admin panel */
        if ( window.location.pathname == '/cp/news/add' ) {
            if (Backbone.history) {
                Backbone.history.start();
                if (this.getCurrentRoute() === '') {
                    require(["apps/news/add/controller"], function(Controller) {
                        Controller.init();
                    });
                }
            }
        }

        /** EDIT News item in admin panel */
        if ( window.location.pathname.match(/^\/cp\/news\/edit\/\d+$/) ) {
            if (Backbone.history) {
                Backbone.history.start();
                if (this.getCurrentRoute() === '') {
                    require(["apps/news/add/controller"], function(Controller) {
                        Controller.init();
                    });
                }
            }
        }

        /** ADD EVENT item in admin panel */
        if ( window.location.pathname == '/cp/events/add' ) {
            if (Backbone.history) {
                Backbone.history.start();
                if (this.getCurrentRoute() === '') {
                    require(["apps/news/add/controller"], function(Controller) {
                        Controller.init();
                    });
                }
            }
        }


    });

    return AppBundle;

});




