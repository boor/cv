/**
 * Created by Alex Stolbov with love
 * Date: 2/11/14
 * Email: boorick@gmail.com
 */

define(["app"], function(AppBundle) {

    /**
     * Entity Module
     */
    AppBundle.module("Entities", function(Entities, AppBundle, Backbone, Marionette, $, _) {

        /**
         * Countdown model entity (check apps/countdown)
         */
        Entities.Countdown = Backbone.Model.extend({
            url: '/api/countdown'
        })

    });

    var API = {
        initState: function() {
            /** Defer object */
            var defer = $.Deferred();

            /** Create new onbject of model */
            var model = new AppBundle.Entities.Countdown();

            /** Fetching data and resolve deferred object if successful */
            model.fetch({
                success: function(data) {
                    defer.resolve(data);
                }
            });

            return defer.promise();
        }
    }



    AppBundle.reqres.setHandler("countdown:init", function() {
        return API.initState();
    });

    return AppBundle.Entities;
});