/**
 * Created by Alex Stolbov with love
 * Date: 11/3/14
 * Email: boorick@gmail.com
 */

SerialManager.module("Entities", function(Entities, SerialManager, Backbone, Marionette, $, _) {

    /**
     * Model for single device (serial)
     */
    Entities.Serial = Backbone.Model.extend({
        urlRoot: "serials",

        /**
         * Validating attributes for Device
         *
         * @param attrs
         * @param options
         * @returns {{}}
         */
        validate: function(attrs, options) {

            var errors = {};

            /** color can not be empty */
            if ( !attrs.color ) {
                errors.color = "Color can not be blank";
            }

            /** status can not be empty */
            if ( !attrs.status ) {
                errors.status = "Status can not be blank";
            }

            /** If error array not empty return it */
            if ( !_.isEmpty(errors) ) {
                return errors;
            }
        }

    });

    /**
     * Collection for Serials of devices
     */
    Entities.SerialCollection = Backbone.Collection.extend({
        url: "serials",
        model: Entities.Serial
    });

    /**
     * Collection for Serials inherited from Paginator, ie paginated collection
     */
    Entities.PaginatedSerials = Backbone.Paginator.requestPager.extend({
        model: Entities.Serial,

        /** Define method for set pattern to collection object from outside.
         * It using for get filtered lists
         */
        setPattern: function($pattern) {
            this.pattern = $pattern;
        },

        /** Define core parameters for the paginated collection */
        paginator_core: {

            /** the type of the request (GET by default) */
            type: 'GET',

            /** the type of reply (jsonp by default) */
            dataType: 'json',

            /** the URL (or base URL) for the service */
            url: '/v2/serials?'
        },

        /** Define UI parameters for the paginated collection */
        paginator_ui: {

            /** the lowest page index API allows to be accessed */
            firstPage: 1,

            /** which page should the paginator start from
             * (also, the actual page the paginator is on)
             */
            currentPage: 0,

            /** how many items per page should be shown */
            perPage: 25,

            /** a default number of total pages to query in case the API or
             * service you are using does not support providing the total
             * number of pages for us
             */
            totalPages: 0,

            totalItems: 0,

            /** Pattern for filtrating collection (empty by default what means is "all") */
            pattern: null
        },

        /** Define backend options for the paginated collection. There is we deciding what parameters
         * and with what values model will send to server for query content.
         */
        server_api: {

            /** number of items to return per request/page */
            'perpage': function() { return this.perPage },

            /** how many results the request should skip ahead to
             * customize as needed. For the Netflix API, skipping ahead based on
             * page * number of results per page was necessary.
             */
            'skip': function() { return this.currentPage * this.perPage },

            'pattern': function() { return this.pattern },

            /** field to sort by */
            'orderby': 'created_at'
        },

        /** Processing response from the server */
        parse: function (response) {
            this.totalPages = Math.ceil(response[0].totalPages / this.perPage);
            this.totalItems = response[0].totalPages;
            return response;
        }
    });


    /**
     * API for the Serial numbers entity
     */
    var API = {

        /** Get collection with serial numbers (devices) list
         * Inside using deferred object from jQuery for dial with the delay
         * between frontend and the backend
         * Return Marionette collection of devices
         */
        getSerialEntities: function() {
            var serials  = new Entities.SerialCollection();
            var defer = $.Deferred();
            serials.fetch({
                success: function(data) {
                    defer.resolve(data);
                }
            });

            return defer.promise();
        },

        /** Get single model entity by id. Deferred objects from jquery
         * Return Marionette Model object
         *
         * @param serialId
         * @returns {*}
         */
        getSerialEntity: function(serialId) {
            var serial = new Entities.Serial({id: serialId});
            var defer = $.Deferred();
            serial.fetch({
                success: function(data) {
                    defer.resolve(data);
                }
            });
            return defer.promise();
        },

        /** Get collection from server and return it.
         * Please remember that returned collection is not Marionette collection. It's
         * inherited from Backbone.Paginator
         *
         * @returns {*}
         */
        getPaginatedSerialEntities: function() {
            var serials = new Entities.PaginatedSerials();
            var defer = $.Deferred();
            serials.fetch({
                success: function(data) {
                    defer.resolve(data);
                }
            });
            return defer.promise();
        }
    };

    /**
     * Setup entity handler for get list of the serials (devices)
     */
    SerialManager.reqres.setHandler("serial:entities", function() {
        return API.getSerialEntities();
    });

    /**
     * Handler for get single device
     */
    SerialManager.reqres.setHandler("serial:entity", function(id) {
        return API.getSerialEntity(id);
    });

    /**
     * Handler for get paginated entities (part of it)
     */
    SerialManager.reqres.setHandler("serial:pentities", function() {
        return API.getPaginatedSerialEntities();
    })

});