/**
 * Created by Alex Stolbov with love
 * Date: 12/3/14
 * Email: boorick@gmail.com
 */

/**
 * Controller for List entity.
 */
SerialManager.module("SerialsApp.List", function(List, SerialManager, Backbone, Marionette, $, _) {

    /**
     * Define Controller for List module (Marionette module)
     */
    List.Controller = {

        /**
         * listSerials method: rendering list of the serial numbers
         */
        listSerials: function() {

            /** Creating and displaying view with spin animation */
            var loadingView = new SerialManager.Common.Views.Loading();
            SerialManager.mainRegion.show(loadingView);

            /** Creating Layout view */
            /** Creating Panel view */
            var serialsListLayout = new List.Layout();
            var serialsListPanel = new List.Panel();
            var serialsListPaginatedPanel = new List.PaginatedPanel();

            /** Quering serial numbers from backend */
            var fetchingSerials = SerialManager.request("serial:pentities");

            $.when(fetchingSerials).done(function(serials) {
                var serialsListView = new List.paginatedSerials({
                    collection: serials
                });


                /**
                 * Including Panel and List Views to Layout
                 */
                serialsListLayout.on("show", function() {
                    serialsListLayout.panelRegion.show(serialsListPanel);
                    serialsListLayout.serialsRegion.show(serialsListView);
                    serialsListLayout.pageRegion.show(serialsListPaginatedPanel);
                });

                serialsListPaginatedPanel.trigger('page:update:info', serials);

                /**
                 * Implementation of click event at paginated panel
                 * Look to View implementation for details
                 */

                /** Click on First page button on the paginated panel */
                serialsListPaginatedPanel.on("page:first", function() {
                    alert("page:first");
                    this.trigger('page:update:info', serials);
                });

                /** Click on Last page button on the paginated panel */
                serialsListPaginatedPanel.on("page:last", function() {
                    alert("page:last");
                    this.trigger('page:update:info', serials);
                });

                /** Click on Previous page button on the paginated panel */
                serialsListPaginatedPanel.on("page:prev", function() {
                    serials.prevPage();
                    this.trigger('page:update:info', serials);
                });

                /** Click on Next page button on the paginated panel */
                serialsListPaginatedPanel.on("page:next", function() {
                    serials.nextPage();
                    this.trigger('page:update:info', serials);
                });

                /** Update information about page
                 * Actually later here need to write code that will generate clasical page buttons
                 * bellow the list of devices
                 */
                serialsListPaginatedPanel.on("page:update:info", function(serials) {
                    this.$el.find('span#js-res-perpage').text(serials.perPage);
                    this.$el.find('span#js-res-total').text(serials.totalItems);
                    this.$el.find('span#js-curr-page').text(serials.currentPage+1);
                });

                /**
                 * Implementation of show item event
                 */
                serialsListView.on("itemview:serial:show", function(childView, model) {
                    SerialManager.trigger("serial:show", model.get("id"));
                });

                /**
                 * Implementation of Delete item event
                 */
                serialsListView.on("itemview:serial:delete", function(childView, model) {
                    model.destroy();
                });

                /**
                 * Implementation of Edit item event
                 */
                serialsListView.on("itemview:serial:edit", function(childView, model) {

                    /** Create new view with modal dialog box (see Edit module for details) */
                    var view = new SerialManager.SerialsApp.Edit.Serial({
                        model: model,
                        asModal: true
                    });

                    /** Processing submit event from dialog view */
                    view.on("form:submit", function(data) {
                        if ( model.save(data) ) {
                            childView.render();
                            SerialManager.dialogRegion.close();
                        } else {
                            view.triggerMethod("form:data:invalid", model.validationError);
                        }
                    })

                    SerialManager.dialogRegion.show(view);

                });

                /**
                 * Implementation of Click "Search" button on tool panel
                 */
                serialsListPanel.on("serials:filter", function(filterCriterion) {
                    /** Get the collection */
                    var collection = serialsListView.options.collection;

                    /** Set pattern for paginated collection. Custom method. */
                    collection.setPattern(filterCriterion);

                    /** Refetch the collection */
                    collection.fetch();
                });


                /** Render Layout to Main Region */
                SerialManager.mainRegion.show(serialsListLayout);
            });


        }
    }
});

