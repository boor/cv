/**
 * Created by Alex Stolbov with love
 * Date: 11/3/14
 * Email: boorick@gmail.com
 */

SerialManager.module("SerialsApp.List", function(List, SerialManager, Backbone, Marionette, $, _) {

    /**
     * General Layout view
     *
     * Create Layout view and defining regions inside
     */
    List.Layout = Marionette.Layout.extend({
        // id of template for the layout
        template: "#serials-list-layout",

        // Define regions, later will put other view there
        regions: {
            panelRegion: "#panel-region",
            serialsRegion: "#serials-region",
            pageRegion: "#paginated-region"
        }
    });



    /**
     * Panel view
     *
     * This view including buttons and Search box
     */
    List.Panel = Marionette.ItemView.extend({
        // ID of template
        template: "#serial-list-panel",

        // Define events for the panel
        events: {
            "submit #filter-form": "filterSerials" // Submit data from Filter form, ie Search
        },

        /**
         * Implementation for submit event
         */
        filterSerials: function(e) {
            e.preventDefault();

            // Get the value of the text filed
            var criterion = this.$(".js-filter-criterion").val();

            // Trigger controller method (see controller method for details)
            this.trigger("serials:filter", criterion);
        }

    });


    /**
     * Paginated panel
     *
     * This view contains navigation controls
     */
    List.PaginatedPanel = Marionette.ItemView.extend({
        // ID of template
        template: "#serial-list-panel-paginated",

        // Events of view (implementations in the Controller Module)
        triggers: {
            "click button.js-page-fast-backward": "page:first", // Click on the First page button
            "click button.js-page-fast-forward": "page:last",   // Click on the Last page button
            "click button.js-page-back": "page:prev",           // Click on the Previous page button
            "click button.js-page-forward": "page:next"         // Click on the Next page button
        }
    });


    /**
     * The Serial view (one row of table)
     *
     * This is a single view for Serial collection of devices. In the fact it's just a single row of table.
     * That view need for collection view
     */
    List.Serial = Marionette.ItemView.extend({
        tagName: "tr",                  // using <tr> tag
        template: "#serial-list-item",  // ID of template for row

        // Define events for table row
        events: {
            "click": "showDeviceDetails",               // Click on the table row
            "click a.js-info": "showDeviceDetail",      // Click on "Info" button
            "click a.js-del": "delDevice",              // Click on "Delete" button
            "click a.js-edit": "editDevice"             // Click on "Edit" button
        },

        /**
         * Implementation if Show device details event
         *
         * @param e
         */
        showDeviceDetails: function(e) {
            e.stopPropagation();
            e.preventDefault();
            this.trigger("serial:show", this.model);
        },

        /**
         * Implementation of Delete device event
         *
         * @param e
         */
        delDevice: function(e) {
            e.stopPropagation();
            e.preventDefault();
            this.trigger("serial:delete", this.model);
        },

        /**
         * Implementation of Edit device event
         *
         * @param e
         */
        editDevice: function(e) {
            e.stopPropagation();
            e.preventDefault();
            this.trigger("serial:edit", this.model);
        },

        /**
         * On render event adding additional class to each row. Class depends of status of the devices just for
         * visual effect
         */
        onRender: function() {
            var rowClass;
            switch(this.model.get('status'))
            {
                case "NEW":
                    rowClass = "success";
                    break;
                case "ACTIVATED":
                    rowClass = "info";
                    break;
                case "DEACTIVATED":
                    rowClass = "error";
                    break;
                case "SERVICE":
                    rowClass = "warning";
                    break;
                default:
                    rowClass = "";
            }

            this.$el.addClass(rowClass);
        }

    });

    /**
     * Composite view for serials
     *
     * Inherited from Marionette CompositeView and containing single serials
     */
    List.Serials = Marionette.CompositeView.extend({
        tagName: "table",                   // using <table> for
        className: "table table-hover",     // adding Bootstrap classes to table
        template: "#serial-list",           // ID for template
        itemView: List.Serial,              // view for single view
        itemViewContainer: "tbody"          // tag for single view

    });


    /**
     * Paginated Composite view
     */
    List.paginatedSerials = Marionette.CompositeView.extend({
        tagName: "table",                   // tag for composite view
        className: "table table-hover",     // additional classes
        template: "#serial-list",           // ID for template
        itemView: List.Serial,              // view for single view
        itemViewContainer: "tbody",         // tag for the single view

        // Trigger Update when view will be render
        onRender: function() {
            this.trigger("page:update:info", this.collection);
        },


        // Serialize data
        serializeData: function(){
            var data = null;

            if (this.model){
                data = this.model.toJSON();
            } else {
                data = this.collection.paginator_ui;
            }

            return data;
        }
    });



});