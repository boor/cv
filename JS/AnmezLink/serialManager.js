/**
 * Created by Alex Stolbov with love
 * Date: 11/3/14
 * Email: boorick@gmail.com
 */


/** Creating a Marionette instance for SerialManager application */
var SerialManager = new Marionette.Application();

/** Define regions for application */
SerialManager.addRegions({
    mainRegion: "#mainRegion",
    dialogRegion: "#dialog-region"
});

/**
 * Add an improved method for navigating
 *
 * @param route
 * @param options
 */
SerialManager.navigate = function(route, options) {
    options || (options == {});
    Backbone.history.navigate(route, options);
}

/**
 * Return current route from application
 *
 * @returns {fragment|*}
 */
SerialManager.getCurrentRoute = function() {
    return Backbone.history.fragment;
}

/**
 *  Application initialization. Doing necessary movings
 */
SerialManager.on("initialize:after", function() {

    if (Backbone.history) {
        Backbone.history.start();

        // If route is empty then trigger List viev for display list of serial numbers
        if (this.getCurrentRoute() === "") {
            SerialManager.trigger("serials:list");
        }
    }

});