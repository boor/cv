/**
 * Created by Alex Stolbov with love
 * Date: 24/3/14
 * Email: boorick@gmail.com
 */

/**
 * Controller for Edit module
 */
SerialManager.module("SerialsApp.Edit", function(Edit, SerialManager, Backbone, Marionette, $, _) {

    Edit.Controller = {

        /**
         * editDevice method
         *
         * Get info from backend, creating edit window and put all data to that window
         *
         * @param id
         */
        editDevice: function(id) {

            /** Creating and displaying view with spin animation */
            var loadingView = new SerialManager.Common.Views.Loading();
            SerialManager.mainRegion.show(loadingView);

            /** Fetching device details from the storage */
            var fetchingSerial = SerialManager.request("serial:entity", id);

            $.when(fetchingSerial).done(function(serial) {
                var view;

                /** If response not empty */
                if (serial !== undefined) {

                    /** Create a Edit view (modal window, see View module for details) */
                    view = new Edit.Serial({
                        model: serial
                    });

                    /**
                     * Implementation of Sumbit event from form
                     */
                    view.on("form:submit", function(data) {
                        if ( serial.save(data) ) {
                            /** saved succesfully, go back to the previous page */
                            window.history.back();
                        } else {
                            /** didnt save the data, show error message */
                            view.triggerMethod("form:data:invalid", serial.validationError);
                        }

                    });


                } else {

                    /** If empty just create empty view with error message */
                    view = new Show.MissingSerial();
                }

                /** Render View to the region */
                SerialManager.mainRegion.show(view);
            })

        } // editDevice

    }; // Edit.controller

}); // module end