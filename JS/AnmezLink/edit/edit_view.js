/**
 * Created by Alex Stolbov with love
 * Date: 24/3/14
 * Email: boorick@gmail.com
 */

/**
 * View module for Edit
 * View based on modal dialog box from jquery.ui
 */
SerialManager.module("SerialsApp.Edit", function(Edit, SerialManager, Backbone, Marionette, $, _) {

    /**
     * Creating ItemView for the device edit form
     */
    Edit.Serial = Marionette.ItemView.extend({
        template: '#device-form',

        events: {
            "change select#device-color": "changeColor",            // Change value of color select element
            "change select#device-status": "changeStatus",          // Change value of status select element
            "click button#js-modal-save": "onFormClickSend",        // Click "Save" button
            "click button#js-modal-cancel": "onFormClickCancel"     // Click "Cancel" button
        },

        /**
         * Initialize the view. Function triggering before user will see it
         * Its great plavce for make several DOM manipulations here
         */
        initialize: function() {
            /** Generating title for the Dialog box */
            this.title = "Edit device (S/N: " + this.model.get('serialNumber') + ")";

            /** Prepare and set device image path. Path based on model and color attributes */
            var image = "/img/devices/" + this.model.get("model") + "." + this.model.get("color") + ".png";
            this.model.set({imagePath: image});
        },

        /**
         * onRender event
         */
        onRender: function() {

        },

        /**
         * onShow event
         */
        onShow: function() {
            var self = this;

            /** if dialog should be modal then create dialog box and binding button events inside
             * according with jquery.ui documentation
             */
            if (this.options.asModal) {
                this.$el.dialog({
                    modal:true,
                    title: this.title,
                    width: "600",
                    resizable: false,
                    buttons: [
                        {text:"Save", click: function() { self.triggerMethod("form:click:send"); }},
                        {text: "Cancel", click: function() { self.$el.dialog("close"); }}
                    ]
                });

            }
        },

        /**
         * Click on Save button on the edit form
         * Binding from dialog constructor
         */
        onFormClickSend: function() {
            var data = Backbone.Syphon.serialize(this)
            this.trigger("form:submit", data);
        },

        onFormClickCancel: function() {

        },


        /**
         * Change color event implementation
         */
        changeColor: function() {
            var self = this;

            /** Get new color value */
            var color = self.$el.find('select#device-color').val();

            /** Get image DOM element */
            var $imageEl = self.$el.find('img.js-edit-image');

            /** Create new path of source picture */
            var newImage = '/img/devices/' + this.model.get("model") + '.' + color + '.png';

            /** Update src attribute on image element */
            $imageEl.attr('src', newImage);
        },

        /**
         * Change status event implementation
         */
        changeStatus: function() {
            var self = this;

            /** Get status select DOM element */
            var $statusEl = self.$el.find('select#device-status');

            /** Get new status value */
            var status = $statusEl.val();

            var elementClass = null;
            switch(status)
            {
                case "NEW":
                    elementClass = "text-success";
                    break;
                case "ACTIVATED":
                    elementClass = "text-info";
                    break;
                case "DEACTIVATED":
                    elementClass = "text-error";
                    break;
                case "SERVICE":
                    elementClass = "text-warning";
                    break;
                default:
                    elementClass = "";
            }

            /** Clean old classes and Set the new class to select element */
            $statusEl.removeClass('text-success text-info text-error text-warning');
            $statusEl.addClass(elementClass);
        },

        onFormDataInvalid: function(errors) {
            var self = this;

            var clearFormErrors = function() {
                var $form = $view.find("form");

                $form.find(".help-inline.error").each(function() {
                    $(this).remove();
                });

                $form.find(".control-group.error").each(function() {
                    $(this).removeClass("error");
                });
            }

            var markErrors = function(value, key) {
                var $controlGroup = self.$el.find("#device-" + key).parent();
                var $errorEl = $("<span>", {class: "help-inline-error", text: value});
                //$controlGroup.find('span').remove();
                $controlGroup.append($errorEl).addClass("error");
            }

            clearFormErrors();
            _.each(errors, markErrors);
        }
    })


});


