/**
 * Created by Alex Stolbov with love
 * Date: 12/3/14
 * Email: boorick@gmail.com
 */

/**
 * Views for Show entity
 */
SerialManager.module("SerialsApp.Show", function(Show, SerialManager, Backbone, Marionette, $, _) {

    /**
     * Show view
     *
     * Shows detailed information of device
     */
    Show.Serial = Marionette.ItemView.extend({
        template: "#serial-view",

        events: {
            "click a.js-back": "clickBackButton"    // Click on "Back" button
        },

        /**
         * Implementation of goBack event
         * @param e
         */
        clickBackButton: function(e) {
            e.stopPropagation();
            e.preventDefault();
            SerialManager.trigger("serials:list");
        }
    });

});