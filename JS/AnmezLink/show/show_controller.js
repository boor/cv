/**
 * Created by Alex Stolbov with love
 * Date: 12/3/14
 * Email: boorick@gmail.com
 */

/**
 * Controller for Show module
 */
SerialManager.module("SerialsApp.Show", function(Show, SerialManager, Backbone, Marionette, $, _) {

    Show.Controller = {

        /**
         * Show serial method
         *
         * Inside fething information from backend (through entity) create view and render it
         *
         * @param {id} id of device that will fetch
         */
        showSerial: function(id) {

            /** Create and render Loader animation */
            var loadingView = new SerialManager.Common.Views.Loading();
            SerialManager.mainRegion.show(loadingView);

            /** Request model */
            var fetchingSerial = SerialManager.request("serial:entity", id);

            $.when(fetchingSerial).done(function(serial) {
                var serialView;

                /** If response is not empty */
                if (serial !== undefined) {

                    /** Create new view (see View module) */
                    serialView = new Show.Serial({
                        model: serial
                    });

                } else {

                    /** If response is empty creating view with error message */
                    serialView = new Show.MissingSerial();
                }

                /** Render view */
                SerialManager.mainRegion.show(serialView);
            })


        } // showSerial method

    }  // Show.Controller

}); // module end