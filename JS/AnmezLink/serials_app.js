/**
 * Created by Alex Stolbov with love
 * Date: 12/3/14
 * Email: boorick@gmail.com
 */

/**
 * SerialApp module for SerialManager application
 * It's the main module for our application and here we are defining global parameters and methods
 * for whole application that works with the serial numbers
 */
SerialManager.module("SerialsApp", function(SerialsApp, SerialManager, Backbone, Marionette, $, _) {

    /**
     * Define routes for application
     */
    SerialsApp.Router = Marionette.AppRouter.extend({
        appRoutes: {
            "serials": "listSerials",
            "serials/:id": "showSerial",
            "serials/:id/edit": "editSerial"
        }
    });

    /**
     * API for application
     */
    var API = {
        /** The global method that shows list of the serial numbers */
        listSerials: function() {
            SerialsApp.List.Controller.listSerials();
        },

        /** Global methos that shows detailed view of the device (single model) */
        showSerial: function(id) {
            SerialsApp.Show.Controller.showSerial(id);
        },

        /** Global method that open dialog window for edit the device */
        editSerial: function(id) {
            SerialsApp.Edit.Controller.editDevice(id);
        }
    }

    /**
     * Shows list view with a serial numbers.
     * serials/list/list_controller.js
     */
    SerialManager.on("serials:list", function() {
        SerialManager.navigate("serials");
        API.listSerials();
    });

    /**
     * Shows detail view of the device
     * apps/serials/show/show_controller.js
     */
    SerialManager.on("serial:show", function(id) {
        SerialManager.navigate("serials/" + id);
        API.showSerial(id);
    });

    /**
     * Navigate to serials/id/edit url and do action
     * apps/serials/edit/edit_controller.js
     */
    SerialManager.on("serial:edit", function(id) {
        SerialManager.navigate("serials/" + id + "/edit");
        API.editSerial(id);
    });

    /**
     * Key press event processing. Event was binding in SerialManager.addInitializer function bellow
     */
    SerialManager.on("general:keypress", function(e) {
        if (e.keyCode == 70) {
            /** "f" key event */
            console.log("There is must be filter window open");
        }

        if (e.keyCode == 65) {
            /** "a" key event */
            console.log("There is Add new device event");
        }
    });

    /**
     * Adding initializer for module. That function will trigger exactly after the global initializer (for all app)
     */
    SerialManager.addInitializer(function() {

        /** Init router for application with API that was define above */
        new SerialsApp.Router({
            controller: API
        });

        /** General keypress event binding for all document */
        $(document).bind('keydown', function (evt){
            SerialManager.trigger("general:keypress", evt);
        });
    })

});