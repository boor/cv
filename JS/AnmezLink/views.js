/**
 * Created by Alex Stolbov with love
 * Date: 13/3/14
 * Email: boorick@gmail.com
 */


/**
 * Views module for SerialManager Application
 * It's containing unclassified views for various situations, like a Spin loader or Dialog box
 */

SerialManager.module("Common.Views", function(Views, SerialManager, Backbone, Marionette, $, _) {

    /**
     * Spinner view. Based on spin.js (//fgnass.github.com/spin.js#v1.3.3)
     */
    Views.Loading = Marionette.ItemView.extend({
        template: "#loading-view",

        onShow: function() {

            /** Parameters for spinner. Here you can change how your spinner will looks like */
            var opts = {
                lines: 13, // The number of lines to draw
                length: 20, // The length of each line
                width: 10, // The line thickness
                radius: 26, // The radius of the inner circle
                corners: 1, // Corner roundness (0..1)
                rotate: 0, // The rotation offset
                direction: 1, // 1: clockwise, -1: counterclockwise
                color: '#0a3c5b', // #rgb or #rrggbb or array of colors
                speed: 1, // Rounds per second
                trail: 68, // Afterglow percentage
                shadow: false, // Whether to render a shadow
                hwaccel: true, // Whether to use hardware acceleration
                className: 'spinner', // The CSS class to assign to the spinner
                zIndex: 2e9, // The z-index (defaults to 2000000000)
                top: '50%', // Top position relative to parent in px
                left: '60%' // Left position relative to parent in px
            };
            $("#spinner").spin(opts);
        }
    });
});