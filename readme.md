# Alexandr Stolbov

Email: [boorick@gmail.com](mailto:boorick@gmail.com)

Skype: a.stolbov

[LinkedIn](https://www.linkedin.com/in/astolbov)


## OBJECTIVE 

Seeking a challenging position as a Web developer in a organization that 
will effectively utilize my experience and offer opportunity to groving up. 


## SKILLS && TECHNOLOGIES

- JavaScript (incl. ECMAScript 2015 (es6))

- React / React Native / BackboneJS / MarionetteJS / etc

- babel / npm / bower / gulp

- Jasmine, Karma

- AMD / RequireJS

- NodeJS

- PHP5 / Laravel Framework 

- HTML5 (cross-device/browser dev) / CSS3 / SASS/LESS, gonna check PostCSS

- PHPDoc, JSDoc, code refactoring, code review

- Strong skills of Linux/BSD System Administration

- Strong knowledge of Networks, VoIP, Communication

- MySQL, PostgreSQL, SQLite, Sphinx, database design

- Git, Subversion, CSV

- APIs integration: Facebook, Google, Twitter, Baidu, Weibo, QQ, Tencent Wechat

- E-commerce, b2b, b2c, CMS, CRM, social applications, SEO

- K.I.S.S., DRY, coding rules, code writing culture



Thanks for your interest.

## PROJECTS

### D2L web-site

[![Foo](https://bytebucket.org/boor/cv/raw/5e8e1da37107d4c34d9baf68a9612c3171777176/images/d2l-thumb.jpg)](https://bytebucket.org/boor/cv/raw/5e8e1da37107d4c34d9baf68a9612c3171777176/images/d2l-full.jpg)

Layout, Coding HTML (Bootstrap), Coding JS (Backbone/Marionette/RequireJS), Coding JS animation, Coding Backend (PHP5, Laravel4)


### VoIP Control Center

[![Foo](https://bytebucket.org/boor/cv/raw/5e8e1da37107d4c34d9baf68a9612c3171777176/images/voip-index-thumb.jpg)](https://bytebucket.org/boor/cv/raw/5e8e1da37107d4c34d9baf68a9612c3171777176/images/voip-index-full.jpg)
[![Foo](https://bytebucket.org/boor/cv/raw/5e8e1da37107d4c34d9baf68a9612c3171777176/images/voip-cdr-thumb.jpg)](https://bytebucket.org/boor/cv/raw/5e8e1da37107d4c34d9baf68a9612c3171777176/images/voip-cdr-full.jpg)
[![Foo](https://bytebucket.org/boor/cv/raw/5e8e1da37107d4c34d9baf68a9612c3171777176/images/voip-vm-thumb.jpg)](https://bytebucket.org/boor/cv/raw/5e8e1da37107d4c34d9baf68a9612c3171777176/images/voip-vm-full.jpg)

UI, Layout, HTML (Bootstrap3), HTML5 Audio, Backbone/Marionette, RequireJS, NodeJS, Websockets, AudioJS, Coding Backend (PHP5, Laravel4, Active Directory Authorization)


### EKD Web-site

[![Foo](https://bytebucket.org/boor/cv/raw/5e8e1da37107d4c34d9baf68a9612c3171777176/images/ekd-thumb.jpg)](https://bytebucket.org/boor/cv/raw/5e8e1da37107d4c34d9baf68a9612c3171777176/images/ekd-full.jpg)

Optimisation for Chinese internet, Bugfixes, PHP coding

### Anmez Link

[![Foo](https://bytebucket.org/boor/cv/raw/d436586aea39ee69b6aca8840ae00144b1baab63/images/amzlink-signin-thumb.jpg)](https://bytebucket.org/boor/cv/raw/d436586aea39ee69b6aca8840ae00144b1baab63/images/amzlink-signin-full.jpg)
[![Foo](https://bytebucket.org/boor/cv/raw/d436586aea39ee69b6aca8840ae00144b1baab63/images/amzlink-serial-thumb.jpg)](https://bytebucket.org/boor/cv/raw/d436586aea39ee69b6aca8840ae00144b1baab63/images/amzlink-serial-full.jpg)
[![Foo](https://bytebucket.org/boor/cv/raw/d436586aea39ee69b6aca8840ae00144b1baab63/images/amzlink-edit-thumb.jpg)](https://bytebucket.org/boor/cv/raw/d436586aea39ee69b6aca8840ae00144b1baab63/images/amzlink-edit-full.jpg)


### eagt.ru Logistic company

[![Foo](https://bytebucket.org/boor/cv/raw/d436586aea39ee69b6aca8840ae00144b1baab63/images/eagt-thumb.jpg)](https://bytebucket.org/boor/cv/raw/d436586aea39ee69b6aca8840ae00144b1baab63/images/eagt-full.jpg)


### Ricci Decor Furniture 

[![Foo](https://bytebucket.org/boor/cv/raw/d436586aea39ee69b6aca8840ae00144b1baab63/images/rucci-thumb.jpg)](https://bytebucket.org/boor/cv/raw/d436586aea39ee69b6aca8840ae00144b1baab63/images/ricci-full.jpg)