<?php
/**
 * Created by Alex Stolbov with love.
 * Date: 3/6/14
 * Email: boorick@gmail.com
 */

class CompanyController extends BaseController
{
    public function __construct(CompanyRepositoryInterface $companies, CategoryRepositoryInterface $categories)
    {
        $this->companies = $companies;
        $this->categories = $categories;
    }


    /**
     * Return a Add Company Form
     *
     * @return mixed
     */
    public function get_new()
    {
        return View::make('company.add', array(
            'categories' => $this->categories->getAll()
        ));
    }

    /**
     * Processing data from Add new Company form
     */
    public function post_new()
    {
        $rules = array(
            'name'  => 'required|unique:companies,title',
            'city'  => 'required',
            'category' => 'required',
            'email' => 'required|email'

        );

        $messages = array(
            'title.required' => 'Не указано название компании',
            'title.unique'   => 'Компания с таким названием уже существует',
            'city'          => 'Город должен быть указан',
            'category'      => 'Не указана сфера деятельности компании',
            'email.email'   => 'Неправильный формат Email адреса'
        );

        $validator = Validator::make(Input::all(), $rules, $messages);

        if ($validator->fails())
        {
            return Redirect::to('/me/company/add')->withErrors($validator);
        }

        $res = $this->companies->add(Auth::User()->id, Input::all());

        return Redirect::to('/me/vacancies/');
    }


    /**
     * Update the title
     *
     * @param $id
     * @return int
     */
    public function update_title($id)
    {
        $title = Input::get('title');

        if ( !$id ) return 0;
        if ( !$title ) return 0;

        $this->companies->update_title($id,$title);
    }

    /**
     * Update the city
     *
     * @param $id
     * @return int
     */
    public function update_city($id)
    {
        $city = Input::get('city');

        if ( !$id ) return 0;
        if ( !$city ) return 0;

        $this->companies->update_city($id,$city);
    }

    /**
     * Update the description
     *
     * @param $id
     * @return int
     */
    public function update_description($id)
    {
        $description = Input::get('description');

        if ( !$id ) return 0;
        if ( !$description ) return 0;

        $this->companies->update_description($id,$description);
    }

}