<?php
/**
 * Created by Alex Stolbov with love.
 * Date: 1/5/14
 * Email: boorick@gmail.com
 */

class CategoryController extends BaseController {

    /**
     * Construct the class and including interfaces
     *
     * @param CategoryRepositoryInterface $categories Category Repository interface
     * @param ResumeRepositoryInterface $resumes Resume category Interface
     * @param VacancyRepositoryInterface $vacancy Vacancy repository interface
     * @param CompanyRepositoryInterface $company company repository interface
     */
    public function __construct(
        CategoryRepositoryInterface $categories,
        ResumeRepositoryInterface $resumes,
        VacancyRepositoryInterface $vacancy,
        CompanyRepositoryInterface $company
    )

    {
        $this->categories = $categories;
        $this->resume = $resumes;
        $this->vacancy = $vacancy;
        $this->company = $company;
    }

    /**
     * List of categories
     *
     * @param $id
     * @return mixed
     */
    public function get_index($id)
    {

        return View::make('pages.category', array(
            'category' => $this->categories->get($id),
            'vacancies' => $this->vacancy->getAll(10),
            'resumes' => $this->resume->getAll(10),
            'vacanciesincategory' => $this->categories->getVacancies($id)
        ));
    }

}