<?php
/**
 * Created by Alex Stolbov with love.
 * Date: 18/6/14
 * Email: boorick@gmail.com
 */

class NewsRepositoryEloquent implements NewsRepositoryInterface
{


    public function parse($url)
    {
        $rss = simplexml_load_file($url);   //Интерпретирует XML-файл в объект

        foreach ($rss->channel->item as $item) {
            echo '<h5>'.$item->title.'</h5>';    //выводим на печать заголовок статьи
            echo $item->description; //выводим на печать текст статьи

        }
    }


    /**
     * Парсим RSS ленту по указанному адресу и выдаем только новые результаты в виде массива
     * @param $url
     * @return array
     */
    public function import($url)
    {
        $rss = simplexml_load_file($url);

        // Parse RSS and if have new item, add it to the array
        foreach ($rss->channel->item as $item) {
            if ( !sizeof(News::where('link','=',$item->link)->get()) ) {
                $array[] = array(
                    'title' => (string)$item->title,
                    'link' => (string)$item->link,
                    'category' => (string)$item->category,
                    'date' => (string)$item->pubDate
                );
            }
        }
        return $array;
    }

    /**
     * Return all models from News
     *
     * @return mixed
     */
    public function getAll()
    {
        return News::orderBy('publish_date','desc')->get();
    }

    /**
     * Insert new object to the News Model
     *
     * @param $data Array of attributes
     * @return string
     */
    public function insert($data)
    {
        $date = date_create_from_format('D, d M Y H:i:s O', $data['date']);
        $news = new News;
        $news->title = $data['title'];
        $news->link = $data['link'];
        $news->publish_date = $date->format('U');
        $news->visible = true;
        return (string)$news->save();
    }

    /**
     * Remove news from database
     *
     * @param $id
     * @return bool
     */
    public function remove($id)
    {
        if ( !$id ) return false;
        News::destroy($id);
        return true;
    }

    public function status($id)
    {
        $news = News::find($id);

        if ( $news->visible  ) {
            $news->visible = false;
            $news->save();
            return 'unvisible';
        } else {
            $news->visible = true;
            $news->save();
            return 'visible';
        }
    }

    public function getLast($limit)
    {
        return News::orderBy('publish_date', 'desc')->take(10)->get();
    }
}