<?php
/**
 * Created by Alex Stolbov with love.
 * Date: 18/6/14
 * Email: boorick@gmail.com
 */

interface NewsRepositoryInterface {

    public function parse($url);
    public function import($url);
    public function getAll();
    public function insert($data);
    public function remove($id);
    public function status($id);
    public function getLast($limit);

}