<?php
/**
 * Created by Alex Stolbov with love.
 * Date: 21/6/14
 * Email: boorick@gmail.com
 */


interface ArticlesRepositoryInterface {

    public function getAll();
    public function getById($id);
    public function add($data,$file);
    public function update($id,$data,$file);
    public function switchStatus($id);
    public function remove($id);
    public function getLast($limit);
}