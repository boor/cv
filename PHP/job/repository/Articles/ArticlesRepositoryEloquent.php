<?php
/**
 * Created by Alex Stolbov with love.
 * Date: 21/6/14
 * Email: boorick@gmail.com
 */

class ArticlesRepositoryEloquent implements ArticlesRepositoryInterface
{


    /**
     * Return all article
     *
     * @return mixed
     */
    public function getAll()
    {
        return Article::orderBy('date', 'desc')->get();
    }


    /**
     * Create new article
     */
    public function add($data,$file)
    {
        $path = '/assets/articles/'; // TODO: Вынести это в ф файл настроек
        $destinationPath = public_path() . $path;
        $fileName = str_random(64) . '.' . $file->getClientOriginalExtension();
        $img = Image::make($file->getRealPath())->widen(300);
        $img->save($destinationPath . '/' . $fileName);

        $article = new Article;
        $article->title = $data['title'];
        $article->description = $data['description'];
        $article->content = $data['content'];
        $article->image = $path . $fileName;
        $article->visible = true;
        $article->noindex = array_key_exists('noindex', $data);
        $article->save();
    }

    /**
     * Get single entity of the article by ID
     *
     * @param $id
     * @return mixed
     */
    public function getById($id)
    {
        return Article::find($id);
    }

    /**
     * Update the article by id
     *
     * @param $id
     * @param $data
     * @param $file
     */
    public function update($id, $data, $file)
    {
        $article = Article::find($id);

        if ($file) {
            // Remove old file
            File::delete(public_path() . $article->image);

            // Processing new one
            $path = '/assets/articles/'; // TODO: Вынести это в ф файл настроек
            $destinationPath = public_path() . $path;
            $fileName = str_random(64) . '.' . $file->getClientOriginalExtension();
            $img = Image::make($file->getRealPath())->widen(300);
            $img->save($destinationPath . '/' . $fileName);

            // And update model attribute
            $article->image = $path . $fileName;
        }

        // Updating other attributes
        $article->title = $data['title'];
        $article->description = $data['description'];
        $article->content = $data['content'];
        $article->noindex = array_key_exists('noindex', $data);
        $article->save();
    }

    /**
     * change status of the article
     *
     * @param $id
     * @return bool
     */
    public function switchStatus($id)
    {
        if ( !$id ) return false;

        $artcl = Article::find($id);

        if ( $artcl->visible ) {
            $artcl->visible = false;
        } else {
            $artcl->visible = true;
        }

        $artcl->save();
        return true;
    }

    /**
     * Remove the article
     *
     * @param $id
     * @return bool
     */
    public function remove($id)
    {
        if ( !$id ) return false;
        Article::destroy($id);
        return true;
    }

    /**
     *  Get last article by created date
     */
    public function getLast($limit)
    {
        return Article::where('visible','=','1')->orderBy('created_at','desc')->take($limit)->get();
    }
}