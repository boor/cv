<?php
/**
 * Created by Alex Stolbov with love.
 * Date: 3/6/14
 * Email: boorick@gmail.com
 */

class CompanyRepositoryEloquent implements CompanyRepositoryInterface
{

    /**
     * Add new company in the model
     *
     * @param $uid
     * @param $data
     * @return Eloquent object
     */
    public function add($uid,$data)
    {
        $name = $data['name'];
        $city = $data['city'];
        $category = $data['category'];
        $email = $data['email'];
        $description = $data['description'];
        $file = $data['file'];

        $path = '';


        /** Processing image */
        if ( Input::file('file') ) {

            $destinationPath = 'assets/companies';
            $destinationFilename = str_random(64) . '.' . Input::file('file')->getClientOriginalExtension();

            $img = Image::make(Input::file('file')->getRealPath())->widen(300);
            $path = $destinationPath . '/' . $destinationFilename;
            $img->save($path);
        }


        /** If company with this title not exist adding new module */
        if ( sizeof(Company::where('title','=','$name')->first()) == 0 ) {
            $company = new Company;
            $company->title = $name;
            $company->city_id = City::firstOrCreate(array('title' => $city))->id;
            $company->category_id = $category;
            $company->email = $email;
            $company->description = $description;
            $company->logo_path = $path;

            $company->save();

            $company->user()->attach($uid);

            return $company->id;
        }

        return false;
    }

    /**
     * Return list of vacancyes witch belong to this company
     *
     * @param $uid ID of target company
     * @return Eloquent object
     */
    public function getVacancyList($uid)
    {
        return Vacancy::where('user_id','=',$uid)->orderBy('position','asc')->get();
        //return Company::find($uid)->vacancies;
    }

    /**
     * Return most recent was updated company
     * @param $limit
     * @return Eloquent object
     */
    public function getLastRecent($limit)
    {
        return Company::orderBy('updated_at','desc')->take($limit)->get();
    }

    /**
     * Update title of the company
     *
     * @param $uid
     * @param $title
     * @return Eloquent object
     */
    public function update_title($uid, $title)
    {
        if ( !$uid ) return false;
        if ( !$title ) return false;

        $company = Company::where('user_id','=',$uid)->first();

        $company->title = $title;

        return $company->save();
    }

    /**
     * Update city of the comapny
     *
     * @param $uid
     * @param $city
     * @return Eloquent object
     */
    public function update_city($uid, $city)
    {
        if ( !$uid ) return false;
        if ( !$city ) return false;

        $company = Company::where('user_id','=',$uid)->first();

        $company->city_id = City::firstOrCreate(array('title' => $city))->id;

        return $company->save();
    }

    /**
     * Update description fo the company
     * @param $uid
     * @param $description
     * @return Eloquent object
     */
    public function update_description($uid, $description)
    {
        if ( !$uid ) return false;
        if ( !$description ) return false;

        $company = Company::where('user_id','=',$uid)->first();
        $company->description = $description;

        return $company->save();
    }
}