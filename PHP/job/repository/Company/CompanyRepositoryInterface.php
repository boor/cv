<?php
/**
 * Created by Alex Stolbov with love.
 * Date: 3/6/14
 * Email: boorick@gmail.com
 */

interface CompanyRepositoryInterface {

    public function add($uid,$data);
    public function getVacancyList($uid);
    public function getLastRecent($limit);

    public function update_title($uid,$title);
    public function update_city($uid,$city);
    public function update_description($uid,$description);
}