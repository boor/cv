<?php
/**
 * Created by Alex Stolbov with love.
 * Date: 1/5/14
 * Email: boorick@gmail.com
 */

class Category extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';


    /**
     * Return two-dimension array of Categories grouped by first character
     *
     * $array['First later if categories'] = Array of Categories started on this letter
     * @return array
     */
    public static function getIndustryList()
    {
        $catlist = Category::orderBy('title','ASC')->get();

        $result = array();
        foreach ($catlist as $item) {
            $sum = sizeof(Vacancy::where('category_id','=',$item->id)->get());
            $result[mb_strcut($item->title,0,2)][] = array(
                'title' => $item->title,
                'id' => $item->id,
                'sum' => $sum
            );
        }

        return $result;
    }


}