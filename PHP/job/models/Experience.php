<?php
/**
 * Created by Alex Stolbov with love.
 * Date: 21/5/14
 * Email: boorick@gmail.com
 */

class Experience extends Eloquent {

    /** @var string Corresponded table */
    protected $table = 'experience';

    protected $guarded = array();

    /**
     * Return title
     *
     * @return mixed
     */
    public function title()
    {
        return $this->belongsToMany('Company');
    }

    /**
     * Return position name
     *
     * @return mixed
     */
    public function position()
    {
        return $this->belongsToMany('Position');
    }


    /**
     * Return Company name title of experience object
     * @return mixed
     */
    public function getTitle()
    {
        return ucfirst(Company::find($this->company_id)->title);
    }

    /**R Return Position title of experience object
     * @return mixed
     */
    public function getPosition()
    {
        return ucfirst(Position::find($this->position_id)->title);
    }

    /**
     * Return DateTime object with date of start working in the current company
     *
     * @return DateTime object
     */
    public function getDateStart()
    {
        $date = new DateTime($this->date_start);
        return $date;
    }

    /**
     * Return DateTime object with date of finish working in the current company
     *
     * @return DateTime object
     */
    public function getDateFinish()
    {
        $date = new DateTime($this->date_finish);
        return $date;
    }


}